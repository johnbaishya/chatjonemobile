import React from "react";
import {TouchableOpacity,Text,ActivityIndicator,View} from "react-native";
import colors from "../../colors";
// import {Icon} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";

const Button = (props)=>{
    var btnColor = colors.primary;
    if(props.success){
        btnColor = colors.success;
    }
    if(props.danger){
        btnColor = colors.danger;
    }
    if(props.info){
        btnColor = colors.info;
    }
    if(props.transparent){
        btnColor=null;
    }
    return(
        <TouchableOpacity
            style={[{
                backgroundColor:props.loading||props.disabled?colors.btnDisabled:btnColor,
                padding:props.small?5:10,
                borderRadius:5,
                marginTop:5,
                marginLeft:5,
                minWidth:props.icon?50:props.full?"100%":100,
                // height:50,
                
                alignItems:"center",
                // opacity:9
                
            },{...props.style}]}
            onPress={props.onPress}
            disabled={props.loading||props.disabled}
        >
            
            {props.icon?
                <React.Fragment>
                    {props.loading?
                        <ActivityIndicator size="small" color={colors.light}/>:
                        <Icon name={props.icon} color={props.transparent?colors.primary:"#fff"} size={20}/>
                    }
                </React.Fragment>:
                <React.Fragment>
                {props.loading?
                    <ActivityIndicator size="small" color={colors.light}/>:
                    <Text style={[{color:colors.light,fontSize:15},{...props.textStyle}]}>{props.title}</Text>
                }
                </React.Fragment>
            }
            {/* <View style={{backgroundColor:"red", height:100, width:"100%",zIndex:9999999}}/> */}
        </TouchableOpacity>
    )
}

export default Button;
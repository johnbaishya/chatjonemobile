import React from "react";
import {View,Text, ActivityIndicator,TouchableOpacity, StyleSheet} from "react-native";
import Modal, { ModalContent,BottomModal,ModalTitle } from 'react-native-modals';
import Button from "../Button";
import myColors from "../../colors";

const eachColor = (onPress,color,index,border,setVisible)=>{
    return(
        <View style={{width:80, marginBottom:10, alignItems:"center"}}>
            <TouchableOpacity 
            key={index}
            onPress={()=>{
                onPress(color);
                setVisible(false);
            }} 
            style={{...styles.eachColor,backgroundColor:color, borderColor:"#000", borderWidth:border?0.5:0}}/>
        </View>
    )
}

const ColorPicker = (props)=>{
    var colors = props.colors?props.colors:[];
    var onPress = props.onPress;
    var visible = props.visible;
    var setVisible = props.setVisible;
    return(
        <BottomModal
            visible={visible}
            onTouchOutside={()=>setVisible(false)}
            modalTitle={<ModalTitle title="Pick a Color"/>}
        >
            <ModalContent>
                {props.loading?
                    <View>
                        <ActivityIndicator color={myColors.primary} size={30}/>
                    </View>:
                    <View style={{width:"100%", flexDirection:"row", justifyContent:"space-between", flexWrap:"wrap", marginTop:20}}>
                        {
                            colors.map((item,index)=>{
                                return(eachColor(onPress,item.color_code,index,props.border,setVisible))
                            })
                        }
                    </View>
                }
            </ModalContent>
        </BottomModal>
    )
}

const styles = StyleSheet.create({
    eachColor:{
        height:50,
        width:50,
        borderRadius:50,
    }
})

export default ColorPicker;
import React, { Component, useState,useEffect } from 'react';
import { Container, Header, Content, Card, CardItem, Body, Right,List,ListItem } from 'native-base';
import Button from "../Button";
import Input from "../Input";
import {setMyDetail,updateMyDetail,cancelUpdateMyDetail} from "../../Redux/action";
import {connect} from "react-redux";
import colors from '../../colors';
import {View,Text,StyleSheet,TouchableOpacity} from "react-native";
import IconMI from "react-native-vector-icons/MaterialIcons";
import ColorPicker from "./../ColorPicker";
import GroupNameChanger from "./../GroupNameChanger";
import {
  getBubbleColors,
  changeBubbleColor,
  getBackgroundColors,
  changeBackgroundColor,
} from "../../Redux/action";


const EachOption = (props)=>{
  var text = props.text;
  var icon = props.icon;
  var iconColor= props.iconColor;
  var onPress = props.onPress;
  return(
    <TouchableOpacity style={{...styles.eachOption,backgroundColor:props.backgroundColor}}
      onPress={onPress}
    >
      <View style={{height:"100%", justifyContent:"center"}}>
        <Text>
          {text}
        </Text>
      </View>
      {
      icon&&
        <View style={{height:"100%", justifyContent:"center", marginLeft:10}}>
          <IconMI name={icon} size={30} color={iconColor}/>
        </View>
      }
    </TouchableOpacity>
  )
}

const ConversationOption = (props)=>{
  const[bblcm, sbblcm] = useState(false);
  const[bgcm, sbgcm] = useState(false);
  const[gnchngr, sgnchngr] = useState(false);
  var scon = props.selectedConversation;
  var bblColor= scon.bubble_color?scon.bubble_color:colors.primary;
  var bgColor = scon.background_color?scon.background_color:"#fff";
  var isGroup = scon.is_group==1;

  var noBubbleColors = props.bubbleColors.colors.length===0;
  var noBackgroundColors = props.backgroundColors.colors.length===0;
    useEffect(()=>{
        if(noBubbleColors){
            props.getBubbleColors();
        }
        if(noBackgroundColors){
            props.getBackgroundColors();
        }
    },[]);
      return(
          <Card style={{width:"100%", marginTop:20}}>
              <CardItem header bordered style={{justifyContent:"space-between",height:50,paddingBottom:10}}>
                <Text style={{color:"#000",fontSize:20}}>Options</Text>
              </CardItem>
              <CardItem>
                <Body>
                  <EachOption text="Change Bubble Color" icon="color-lens" iconColor={bblColor} onPress={()=>{sbblcm(true)}} />
                  <EachOption text="Change background Color" backgroundColor={bgColor} onPress={()=>{sbgcm(true)}}/>
                  {isGroup&&
                    <React.Fragment>
                    <EachOption text="Change Group Name" icon="edit" iconColor={bblColor} onPress={()=>{sgnchngr(true)}}/>
                    <EachOption text="Add members" onPress={()=>{props.navigation.navigate("AddMembers")}}/>
                    <EachOption text="Show members" onPress={()=>{props.navigation.navigate("DisplayMembers")}}/>
                  </React.Fragment>
                  }
                </Body>
              </CardItem>
              <ColorPicker 
                visible={bblcm} 
                setVisible={sbblcm}
                colors={props.bubbleColors.colors}
                loading={props.bubbleColors.loading}
                onPress={(color)=>props.changeBubbleColor(scon,color)}
              />
              <ColorPicker 
                visible={bgcm} 
                setVisible={sbgcm}
                colors={props.backgroundColors.colors}
                loading={props.backgroundColors.loading}
                onPress={(color)=>props.changeBackgroundColor(scon,color)}
                border
              />
              <GroupNameChanger
                visible={gnchngr} 
                setVisible={sgnchngr}
              />
           </Card>
      )
}

const styles = StyleSheet.create({
  eachRow:{
    width:"100%",
    flexDirection:"row",
    marginBottom:5
  },
  leftItem:{
    width:"30%"
  },
  rightItem:{
    width:"70%"
  },
  eachOption:{
    width:"100%", 
    height:50, 
    // borderBottomColor:"#000",
    // borderBottomWidth:1,
    flexDirection:"row",
    paddingLeft:10,
    borderRadius:10
  }
})

const mapStateToProps = (state)=>{
    const {profileReducer,messageReducer,bubbleColors,backgroundColors} = state;
    return {
      profileReducer,
      selectedConversation:messageReducer.selectedConversation,
      bubbleColors,
      backgroundColors,
    };
}


export default connect(mapStateToProps,{
    setMyDetail,
    updateMyDetail,
    cancelUpdateMyDetail,
    getBubbleColors,
    changeBubbleColor,
    getBackgroundColors,
    changeBackgroundColor
})(ConversationOption);
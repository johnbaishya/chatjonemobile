import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Text, Body, Right } from 'native-base';
import Button from "./../Button";
import Input from "../Input";
import {setMyDetail,updateMyDetail,cancelUpdateMyDetail} from "../../Redux/action";
import {connect} from "react-redux";
import colors from '../../colors';


const updateProfile = (props)=>{
  var pdata = {
      name:props.name,
      email:props.email,
      phone:props.phone,
  }

  props.updateMyDetail(pdata);
}
const Detail = (props)=>{
    return(
        <Card style={{width:"100%", marginTop:20}}>
            <CardItem header bordered style={{justifyContent:"space-between",height:50,paddingBottom:10}}>
              <Text style={{color:"#000"}}>About</Text>
              {/* <Right style={{alignContent:"flex-end"}}> */}
                <Button icon="edit" style={{alignSelf:"flex-end"}} small transparent onPress = {()=>{props.setMyDetail({editable:true})}}/>
              {/* </Right> */}
            </CardItem>
            <CardItem>
              <Body>
                <Input placeholder="name" 
                  value = {props.name} 
                  title="Name" disabled={!props.editable}
                  onChangeText={(e)=>{props.setMyDetail({name:e})}}
                  />
                <Input placeholder="email" value={props.email} title="email" disabled={!props.editable}
                  onChangeText={(e)=>{props.setMyDetail({email:e})}}
                />
                <Input placeholder="phone" value={props.phone} title="Phone" disabled={!props.editable}
                  onChangeText={(e)=>{props.setMyDetail({phone:e})}}
                />
              </Body>
            </CardItem>
            {props.editable&&
            <CardItem footer style={{justifyContent:"flex-end"}}>
              <Button title="cancel" small 
              style={{backgroundColor:colors.btnDisabled}}
              onPress={()=>{props.cancelUpdateMyDetail()}}
              />
              <Button title="save" small
                onPress={()=>{updateProfile(props)}}
                loading={props.loading}
              />
            </CardItem>}
         </Card>
    )
}

const mapStateToProps = (state)=>{
    const {profileReducer} = state;
    return profileReducer;
}


export default connect(mapStateToProps,{
    setMyDetail,
    updateMyDetail,
    cancelUpdateMyDetail
})(Detail);
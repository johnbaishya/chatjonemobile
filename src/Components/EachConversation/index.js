import React from "react";
import {Text,TouchableOpacity,View} from "react-native";
import url from "./../../url";
import colors from "./../../colors";
import ProfilePicture from "./../ProfilePicture";


const ConInfo = (name,message,senderId)=>{
    return (
        <View style={{
            height:"100%",
            width:"80%",
            borderBottomColor:colors.borderColor,
            borderBottomWidth:0.3,
            marginLeft:10,
            justifyContent:"center"
        }}>
            <Text style={{fontWeight:"bold", fontSize:17}}>{name}</Text>
            <Text style={{
                color:senderId==global.userId?colors.lightText:"#000",
                // color:colors.lightText,
                fontWeight:senderId==global.userId?"normal":"bold"
                }}>{message}</Text>
        </View>
    )
}

const EachConversation = (props)=>{
    var data = props.data;
    var isGroup = data.is_group;
    var conversationName = isGroup==1?data.name?data.name:"new Group":data.partner.name;
    var conversationPP = isGroup==1?data.profile_image:data.partner.profile_img;
    var senderId = data.sent_by;
    var lastMessage = data.last_message;
    var lastMessageType = data.last_message_type;
    if(lastMessageType == "image"){
        if(data.sent_by==global.userId){
            lastMessage = "You sent an Image";
        }else{
            lastMessage ="You recieved an Image";
        }
    }
    if(lastMessage){
        if(lastMessage.length > 30){
            lastMessage = lastMessage.substring(0,29);
            lastMessage=lastMessage+"...";
       };
    }else{
        lastMessage= "No message yet";
    }
    
    var pp = conversationPP?data.local?conversationPP:url.baseUrl+conversationPP:"";
    return (
        <TouchableOpacity style={{
            width:"90%",
            height:75,
            flexDirection:"row",
            padding:5,
            alignSelf:"center"
        }}
        onPress={props.onPress}
        >
            <View style={{width:"20%", height:"100%", justifyContent:"center"}}>
                <ProfilePicture src={pp} isGroup={isGroup==1}/>
            </View>
            {ConInfo(conversationName,lastMessage,senderId)}
        </TouchableOpacity>
    )
}

export default EachConversation;
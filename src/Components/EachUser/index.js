import React from "react";
import {Text,TouchableOpacity,View} from "react-native";
import url from "./../../url";
import colors from "./../../colors";
import ProfilePicture from "./../ProfilePicture";

const ConInfo = (name,message,senderId)=>{
    return (
        <View style={{
            height:"100%",
            width:"80%",
            borderBottomColor:colors.borderColor,
            borderBottomWidth:0.3,
            marginLeft:10,
            justifyContent:"center"
        }}>
            <Text style={{fontWeight:"bold", fontSize:17}}>{name}</Text>
            <Text style={{
                color:senderId==global.userId?colors.lightText:"#000",
                // color:colors.lightText,
                fontWeight:senderId==global.userId?"normal":"bold"
                }}>{message}</Text>
        </View>
    )
}

const EachUser = (props)=>{
    var user = props.user;
    var name = props.user.name;
    var pp = user.profile_img?url.baseUrl+user.profile_img:"";
    return (
        <TouchableOpacity style={{
            width:"90%",
            height:50,
            alignSelf:"center",
            flexDirection:"row",
            padding:5,
            backgroundColor:user.added&&"lightgreen",
        }}
        onPress={props.onPress}
        >
            <View style={{width:"10%", height:"100%", justifyContent:"center"}}>
                <ProfilePicture src={pp} size={30} isGroup={false}/>
            </View>
            <View style={{
                height:"100%",
                width:"90%",
                // borderBottomColor:colors.borderColor,
                // borderBottomWidth:0.3,
                marginLeft:10,
                justifyContent:"center"
            }}>
                <Text style={{fontWeight:"bold", fontSize:17}}>{name}</Text>
            </View>
            {/* {ConInfo(conversationName,lastMessage,senderId)} */}
        </TouchableOpacity>
    )
}

export default EachUser;
import React,{useState} from "react";
import {View,Text, ActivityIndicator,TouchableOpacity, StyleSheet} from "react-native";
import Modal, { ModalContent,BottomModal,ModalTitle } from 'react-native-modals';
import Button from "../Button";
import myColors from "../../colors";
import Input from "../Input";
import {connect} from "react-redux";
import {changeGroupName,createGroup} from "../../Redux/action";
import colors from "../../colors";
import IconMI from "react-native-vector-icons/MaterialIcons";


const GroupCreator = (props)=>{
    const [modal, setModal] = useState(false);
    var sc = props.messageReducer.selectedConversation;
    var btnColor =sc.bubble_color?sc.bubble_color:colors.primary;
    const [gName,setGname] = useState("");
    let gData = {name:gName};
    return(
        <React.Fragment>
            <TouchableOpacity style={{
                backgroundColor:colors.primary,
                height:60,
                width:60,
                position:"absolute",
                bottom:20,
                right:20,
                borderRadius:60,
                alignItems:"center",
                justifyContent:"center"
                }}
                    onPress={()=>{setModal(true)}}
                >
                    <IconMI name="group-add" color="#fff" size={40}/>
            </TouchableOpacity>
            <BottomModal
                visible={modal}
                onTouchOutside={()=>{
                    setModal(false);
                    setGname("");
                }}
                modalTitle={<ModalTitle title="Create New Group"/>}
            >
                <ModalContent>
                        <View>
                        <Input placeholder="Type New Group Name" value={gName} onChangeText={(text)=>{setGname(text)}}/>
                        </View>
                        <View style={{flexDirection:"row", justifyContent:"flex-end"}}>
                            <Button title="cancel" 
                            onPress={()=>{
                                setModal(false);
                                setGname("");
                            }} 
                            style={{backgroundColor:colors.btnDisabled}}
                            />
                            <Button title="Create" style={{backgroundColor:btnColor}}
                                onPress={()=>{
                                    props.createGroup(gData);
                                    setGname("");
                                    setModal(false);
                                }}
                            />
                        </View>
                </ModalContent>
            </BottomModal>
        </React.Fragment>
    )
}

const styles = StyleSheet.create({
    eachColor:{
        height:50,
        width:50,
        borderRadius:50,
    }
})

const mapStateToProps = (state)=>{
    const {messageReducer} = state;
    return  {
        messageReducer,
    }
}

export default connect(
    mapStateToProps,{
        changeGroupName,
        createGroup
    }
)(GroupCreator);
// export default GroupNameChanger;
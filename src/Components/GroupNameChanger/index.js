import React,{useState} from "react";
import {View,Text, ActivityIndicator,TouchableOpacity, StyleSheet} from "react-native";
import Modal, { ModalContent,BottomModal,ModalTitle } from 'react-native-modals';
import Button from "../Button";
import myColors from "../../colors";
import Input from "./../Input";
import {connect} from "react-redux";
import {changeGroupName} from "../../Redux/action";
import colors from "../../colors";


const GroupNameChanger = (props)=>{
    var sc = props.messageReducer.selectedConversation;
    var btnColor =sc.bubble_color?sc.bubble_color:colors.primary;
    const [gName,setGname] = useState("");
    var visible = props.visible;
    var setVisible = props.setVisible;
    return(
        <BottomModal
            visible={visible}
            onTouchOutside={()=>setVisible(false)}
            modalTitle={<ModalTitle title="Group Name"/>}
        >
            <ModalContent>
                    <View>
                       <Input placeholder="Type New Group Name" value={gName} onChangeText={(text)=>{setGname(text)}}/>
                    </View>
                    <View style={{flexDirection:"row", justifyContent:"flex-end"}}>
                        <Button title="cancel" 
                        onPress={()=>{
                            setVisible(false);
                            setGname("");
                        }} 
                        style={{backgroundColor:colors.btnDisabled}}
                        />
                        <Button title="Save" style={{backgroundColor:btnColor}}
                            onPress={()=>{
                                props.changeGroupName(sc,gName);
                                setGname("");
                                setVisible(false);
                            }}
                        />
                    </View>
            </ModalContent>
        </BottomModal>
    )
}

const styles = StyleSheet.create({
    eachColor:{
        height:50,
        width:50,
        borderRadius:50,
    }
})

const mapStateToProps = (state)=>{
    const {messageReducer} = state;
    return  {
        messageReducer,
    }
}

export default connect(
    mapStateToProps,{
        changeGroupName,
    }
)(GroupNameChanger);
// export default GroupNameChanger;
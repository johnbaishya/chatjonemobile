import React from "react";
import {TextInput,View,Text} from "react-native";
import colors from "./../../colors";

const MyTextInput = (props) => {
    return(
        <View style={{flexDirection:"row"}}>
            {props.title&&<Text style={{width:"25%", marginTop:20}}>{props.title}</Text>}
            <TextInput 
                editable={!props.disabled}
                value={props.value&&props.value}
                onChangeText = {props.onChangeText}
                placeholder={props.placeholder}
                secureTextEntry={props.password}
                autoCapitalize="none"
                style={[
                    {
                        borderBottomColor:colors.primary,
                        borderBottomWidth:props.disabled?0:1,
                        borderRadius:5,
                        height:40,
                        width:props.title?"72%":"100%",
                        marginTop:10,
                        marginBottom:10,
                    },
                    {...props.style}
                ]}
                keyboardType={props.type}
            />
        </View>
    )
}

export default MyTextInput;
import React from "react";
import ContentLoader, {BulletList,Rect} from 'react-content-loader/native';
import ProfilePic from "./../ProfilePicture";
import {View} from "react-native";

export const SingleUserLoader = () => (
    <ContentLoader
    //   style={{marginTop:0}}
      height={50}
      speed={1}
      backgroundColor={'#aeb4b7'}
      foregroundColor={'#e4e6e7'}
      viewBox="30 0 380 70"
    >
      {/* Only SVG shapes */}
      <Rect x="0" y="0" rx="40" ry="40" width="70" height="70" />
      <Rect x="100" y="25" rx="4" ry="4" width="200" height="15" />
      <Rect x="100" y="50" rx="3" ry="3" width="150" height="10" />
    </ContentLoader>
  )

  export const UserListLoader = ()=>(
            [...Array(6)].map((item,index)=>{
            return(
                <View style={{marginLeft:0, marginTop:10, marginBottom:20}} key={index}>
                    <SingleUserLoader/>
                </View>
            )
            })
  )

  export const SuggestedUserLoader =()=>(
    <ContentLoader
    //   style={{marginTop:0}}
      height={35}
      speed={1}
      backgroundColor={'#aeb4b7'}
      foregroundColor={'#e4e6e7'}
      viewBox="65 0 380 70"
    >
      {/* Only SVG shapes */}
      <Rect x="0" y="0" rx="40" ry="40" width="65" height="65" />
      <Rect x="80" y="15" rx="4" ry="4" width="300" height="20" />
      {/* <rect x="80" y="45" rx="3" ry="3" width="150" height="10" /> */}
    </ContentLoader>
  )

  export const SuggestedUserListLoader = ()=>(
    [...Array(8)].map((item,index)=>{
    return(
        <View style={{marginTop:10, marginBottom:20}} key={index}>
            <SuggestedUserLoader/>
        </View>
    )
    })
)

export const UserDetailLoader = ()=>(
    <React.Fragment>
        <ProfilePic src={""} extraLarge/>
        {/* <h3>{currentUser.name}</h3><br/> */}
        <BulletList/>
        {/* <Options/> */}
    </React.Fragment>
)

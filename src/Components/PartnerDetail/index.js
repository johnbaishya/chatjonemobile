import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Body, Right } from 'native-base';
import Button from "../Button";
import Input from "../Input";
import {setMyDetail,updateMyDetail,cancelUpdateMyDetail} from "../../Redux/action";
import {connect} from "react-redux";
import colors from '../../colors';
import {View,Text,StyleSheet} from "react-native";


const PartnerDetail = (props)=>{
    var con = props.conversation;
    var partner = con.partner;
    var isGroup = con.is_group==1;
    if(isGroup){
      return null;
    }else{
      return(
          <Card style={{width:"100%", marginTop:20}}>
              <CardItem header bordered style={{justifyContent:"space-between",height:50,paddingBottom:10}}>
                <Text style={{color:"#000",fontSize:20}}>About</Text>
              </CardItem>
              <CardItem>
                <Body>
                  <View style={styles.eachRow}>
                    <Text style={styles.leftItem}>Name</Text>
                    <Text style={styles.rightItem}>{partner.name}</Text>
                  </View>
                  <View style={styles.eachRow}>
                    <Text style={styles.leftItem}>Email</Text>
                    <Text style={styles.rightItem}>{partner.email}</Text>
                  </View>
                  <View style={styles.eachRow}>
                    <Text style={styles.leftItem}>Phone</Text>
                    <Text style={styles.rightItem}>{partner.phone}</Text>
                  </View>
                </Body>
              </CardItem>
           </Card>
      )
    }
}

const styles = StyleSheet.create({
  eachRow:{
    width:"100%",
    flexDirection:"row",
    marginBottom:5
  },
  leftItem:{
    width:"30%"
  },
  rightItem:{
    width:"70%"
  }
})

const mapStateToProps = (state)=>{
    const {profileReducer} = state;
    return profileReducer;
}


export default connect(mapStateToProps,{
    setMyDetail,
    updateMyDetail,
    cancelUpdateMyDetail
})(PartnerDetail);
import React from "react";
import {View,Image,Text} from "react-native";
import upp from "./../../Images/pp.png";
import gpp from "./../../Images/pp-group.png";
import colors from "../../colors";

const ProfilePicture = (props) =>{
    var size = props.size?props.size:50;
    var noImage = props.isGroup?gpp:upp;
    var imgUrl=props.src?{uri:props.src}:noImage;
    return(
        <View style = {{height:size,width:size, borderRadius:size, backgroundColor:colors.light}}>
            <Image source={imgUrl} style={{height:size,width:size,borderRadius:size}}/>
        </View>
        // <Text>image</Text>
    )
}

export default ProfilePicture;
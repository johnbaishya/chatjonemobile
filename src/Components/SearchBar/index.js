import React from "react";
import {TextInput,View,TouchableOpacity,Keyboard} from "react-native";
import {Icon} from "native-base";
import colors from "./../../colors";
import {changeUserSearchText} from "./../../Redux/action";
import {connect} from "react-redux";


const SearchBar = (props) => {
    return(
        <View
            style={{
                borderColor:colors.borderColor,
                borderWidth:0.5,
                borderRadius:20,
                height:40,
                width:"100%",
                marginTop:10,
                marginBottom:10,
                flexDirection:"row",
                paddingLeft:10,
                paddingRight:10,
            }}
        >
            <View style={{width:"10%",height:"100%",justifyContent:"center"}}>
                <Icon name="search" style={{color:colors.lightText}} color={colors.lightText}/>
            </View>
            <TextInput 
                value={props.value}
                onChangeText = {props.onChangeText}
                placeholder={props.placeholder}
                secureTextEntry={props.password}
                autoCapitalize="none"
                style={[
                    {
                        height:"100%",
                        width:"80%",
                    },
                    {...props.style}
                ]}
                keyboardType={props.type}
            />
            {props.showCross&&
            <TouchableOpacity style={{width:"10%",height:"100%",justifyContent:"center"}} 
                onPress={()=>{
                    props.changeUserSearchText("");
                    Keyboard.dismiss();
                }}
            >
                <Icon name="close" style={{color:colors.lightText}} color={colors.lightText}/>
            </TouchableOpacity>
            }
        </View>
    )
}

export default connect(null,{changeUserSearchText})(SearchBar);
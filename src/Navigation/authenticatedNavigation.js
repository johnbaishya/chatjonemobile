import React,{useEffect} from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MainScreen from "./../Screens/MainScreen";
import ChatScreen from "./../Screens/Chatscreen";
import ConversationDetail from "../Screens/ConversationDetail";
import DisplayMembers from "./../Screens/DisplayMembers";
import AddMembers from "./../Screens/AddMembers";
import {setMyDetail,setListener,setListener2} from "./../Redux/action";
import AsyncStorage from "@react-native-community/async-storage";
import {connect} from "react-redux";
import Axios from "axios";
import notificationService from "../Services/pushNotification";
// import 

const InitialMethods = async(props)=>{
    console.log('inside initial method');
    var token = await AsyncStorage.getItem('@token');
    console.log(token);
    if(token){
        console.log("has token");
        var user = await AsyncStorage.getItem('@user');
        user = JSON.parse(user);
        var {id} = user;
        console.log(user);
        props.setMyDetail(user);
        global.userId = id;
        global.user = user;
        Axios.defaults.headers.common = {
            'Authorization': 'Bearer ' + token,
            'Accept': "application/json",
            'Content-Type': "application/json"
          }
        props.setListener(id);
        props.setListener2(id);
        notificationService();
    }
}

const Stack = createStackNavigator();
const AuthenticatedNavigation = (props)=>{
    var authenticated = props.authenticateReducer.authenticated;
    useEffect(()=>{
        InitialMethods(props);
    },[authenticated]);
    return(
            <Stack.Navigator screenOptions={{headerShown:false}}>
                <Stack.Screen name="MainScreen" component={MainScreen}/>
                <Stack.Screen name="ChatScreen" component={ChatScreen}/>
                <Stack.Screen name="ConversationDetail" component={ConversationDetail}/>
                <Stack.Screen name="DisplayMembers" component={DisplayMembers}/>
                <Stack.Screen name="AddMembers" component={AddMembers}/>
            </Stack.Navigator>
    )
}

const mapStateToProps = ({authenticateReducer})=>{
    return{
        authenticateReducer,
    }
}

// export default MainNavigation;
export default connect(mapStateToProps,{
    setMyDetail,
    setListener,
    setListener2,
})(AuthenticatedNavigation);
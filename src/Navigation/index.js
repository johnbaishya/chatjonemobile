import React, { useEffect } from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AuthencatedNavigation from "./authenticatedNavigation";
import UnauthenticatedNavigation from "./unauthenticatedNavigation";
import {connect} from "react-redux";
import SplasScreen from "./../Screens/SplashScreen";
import AsyncStorage from '@react-native-community/async-storage';
import Axios from "axios";
import {changeAuthentication,setMyDetail} from "./../Redux/action";
import getTheme  from "../../native-base-theme/components";
import material from "../../native-base-theme/variables/platform";
import {StyleProvider} from "native-base";

const checkStatus = async(props)=>{
    var token = await AsyncStorage.getItem('@token');
    console.log(token);
    if(token){
        var user = await AsyncStorage.getItem('@user');
        user = JSON.parse(user);
        var {id} = user;
        props.setMyDetail(user);
        global.userId = id;
        global.user = user;
        Axios.defaults.headers.common = {
            'Authorization': 'Bearer ' + token,
            'Accept': "application/json",
            'Content-Type': "application/json"
          }
        props.changeAuthentication(true,false);
    }else{
        props.changeAuthentication(false,false);
    }
}

const Stack = createStackNavigator();

const MainNavigation = (props)=>{
    var authenticated = props.authenticateReducer.authenticated;
    var loading = props.authenticateReducer.loading;
    useEffect(()=>{
        checkStatus(props);
    },[]);
    if(loading){
        return(
            <SplasScreen/>
        )
    }
    return(
        <StyleProvider style={getTheme(material)}>
        <NavigationContainer>
            <Stack.Navigator screenOptions={{headerShown:false}}>
                {authenticated?
                    <Stack.Screen name="AuthenticatedNavigation" component={AuthencatedNavigation}/>:
                    <Stack.Screen name="UnauthenticatedNavigation" component={UnauthenticatedNavigation}/>
                }
            </Stack.Navigator>
        </NavigationContainer>
        </StyleProvider>
    )
}

const mapStateToProps = ({authenticateReducer})=>{
    return{
        authenticateReducer,
    }
}

// export default MainNavigation;
export default connect(mapStateToProps,{
    changeAuthentication,
    setMyDetail,
})(MainNavigation);
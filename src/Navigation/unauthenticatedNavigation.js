import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Login from "../Screens/Login";
import Signup from "../Screens/Signup";
import {connect} from "react-redux";
// import 

const Stack = createStackNavigator();
const UnauthenticatedNavigation = (props)=>{
    var authenticated = props.authenticateReducer.authenticated;
    return(
            <Stack.Navigator screenOptions={{headerShown:false}}>
                <Stack.Screen name="Login" component={Login}/>
                <Stack.Screen name="Signup" component={Signup}/>
            </Stack.Navigator>
    )
}

const mapStateToProps = ({authenticateReducer})=>{
    return{
        authenticateReducer,
    }
}

// export default MainNavigation;
export default connect(mapStateToProps)(UnauthenticatedNavigation);
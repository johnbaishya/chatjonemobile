import {
  SET_LOGIN_CREDENTIALS, 
  CLEAR_CREDENTIALS, 
  SIGNUP_CREDENTIALS,
  GET_USER_DETAIL,
  GET_INITIAL_CONVERSATION,
  MESSAGES,
  ADD_MESSAGES,
  SEARCH_USER,
  CHANGE_SEARCH_USER_TEXT,
  DETERMINE_FIRST_MESSAGE,
  MESSAGE_LOADER,
  UPDATE_CONVERSATION_LIST,
  SET_MY_DETAIL,
  SUGGESTED_USERS,
  SHOW_SUGGESTED_USERS,
  GET_BUBBLE_COLORS,
  GET_BACKGROUND_COLORS,
  REPLACE_LOADED_CONVERSATION,
  SHOW_GROUP_CREATOR,
  UPDATE_NEW_GROUP_NAME,
  CREATE_GROUP,
  SELECT_FAKE_CONVERSATION,
  SEARCH_MEMBER_FOR_GROUP,
  CHANGE_AUTHENTICATION,
  CLEAR_STORE,
  CHANGE_APP_SETTING
} from "./actionTypes";
// import cookie from "react-cookies";
import Axios from "axios";
import url from "./../url";
import Echo from "laravel-echo";
import Pusher from "pusher-js/react-native";
import AsyncStorage from "@react-native-community/async-storage";
import colors from "../colors";
let nextTodoId = 0;
var notificationCount = 0;
global.Pusher = Pusher;

var eco = new Echo({
  broadcaster:'pusher',
  key:url.pusherKey,
  cluster: 'ap2',
  forceTLS: true,
});


export const setListener = (uid)=>{
  console.log("setListener-"+uid);
  return (dispatch)=>{
    eco.channel('Message-'+uid)
    .listen('.chat',(e)=>{
      console.log(e);
      dispatch(addMessage(e.data));
    } )
  }
}


export const setListener2 = (uid)=>{
  return (dispatch)=>{
    eco.channel('Conversation-'+uid)
    .listen('.chat',(e)=>{
      console.log(e);
      var con = e.data;
      dispatch(updateConversationList(con));
      dispatch(replaceLoadedConversation(con));
    })
  }
}

export const logout = (uid)=>{
    console.log('inside logout');
    eco.leave('Message-'+uid);
    eco.leave('Conversation-'+uid);
    var fd = new FormData();
    fd.append("device_token",global.deviceToken);
    Axios.post(url.apiUrl+"remove-device-token",fd);
    return async(dispatch)=>{
      await AsyncStorage.clear();
      dispatch(changeAuthentication(false,false));
      dispatch(clearStore());
    }
}


export const showBadgeonTab = ()=>{
  notificationCount++;
  var newTitle = "🔴 "+title;
  // document.title = newTitle;
}

export const showRealTitleListener   = ()=>{
  window.addEventListener('focus',function(){
    // document.title="Chatjone";
  })
}

// export const playSound = ()=>{
//   if (document.readyState === "complete"){
//     var audio = new Audio('/sound/notification.mp3');
//     audio.play();
//   }
// }

// export const playNotificationSound = ()=>{
//   var documentHidden = document.hidden;
//   if(documentHidden){
//     playSound();
//     showBadgeonTab();
//   }
// }

// export const changeAppSetting = async(key,value) =>{
//   var data = await AsyncStorage.getItem('@setting');
//   data = JSON.parse(data);
//   data[key] = value;
//   var newData = JSON.stringify(data);
//   await AsyncStorage.setItem('@setting',newData);
//   return{
//     type: CHANGE_APP_SETTING,
//     payload: {
//       key,
//       value,
//     }
//   }
// }

// export const getInitialSetting =()=>{
//   return async(dispatch)=>{
//     var data = await AsyncStorage.getItem('@setting');
//     if(data){
//       data = JSON.parse(data);
//       dispatch(changeAppSetting("primaryColor",data.primaryColor));
//     }else{
//       dispatch(changeAppSetting("primaryColor",colors.primary));
//     }
//   }
// }

export const setCrendentials = content => ({
  type: SET_LOGIN_CREDENTIALS,
  payload: {
    id: ++nextTodoId,
    email:content.email,
    password:content.password,
  }
});

export const setSignupCredentials = content => ({
  type: SIGNUP_CREDENTIALS,
  payload: {
    id: ++nextTodoId,
    name:content.name,
    email:content.email,
    phone:content.phone,
    password:content.password,
  }
});

// export const showConversation = ()=>{
//   document.getElementById("left").style.zIndex = 99;
//   document.getElementById("main").style.zIndex = 5;
// }

// export const showMessages = ()=>{
//   var smallScreen = window.matchMedia("(orientation:portrait )").matches;
//   if(smallScreen){
//     document.getElementById("left").style.zIndex = 5;
//     document.getElementById("main").style.zIndex = 9;
//   }
// }

export const clearCrendials = id => ({
  type: CLEAR_CREDENTIALS,
  payload: {
      id:++nextTodoId,
      email:"",
      password:"",
   }
});



export const setInitialConversation = (data,loading) =>{
  return{
    type:GET_INITIAL_CONVERSATION,
    payload: {
      data:data.data,
      currentPage:data.current_page,
      lastPage:data.last_page,
      loading:loading,
    }
  }
}

export const selectFakeConversation = (conversation)=>{
  return{
    type:SELECT_FAKE_CONVERSATION,
    payload:{
      conversation,
    }
  }
}

export const updateConversationList = (conversation)=>{
  return{
    type:UPDATE_CONVERSATION_LIST,
    payload: {
      conversation,
    }
  }
}

export const getInitialConversation = () => {
  return (dispatch)=>{
    return Axios.get(url.apiUrl+"conversation")
    .then(res=>{
        dispatch(setInitialConversation(res.data,false))
    })
    .catch(err=>{
      console.log(err);
      // throw(err);
      console.log(err.response);
    })
  }
}

export const selectUser = (allUser,selectedUser,loading) =>{
  return{
    type:GET_USER_DETAIL,
    payload: {
      allUser,
      selectedUser,
      loading
    }
  }
}

export const getUserDetail = (allUser,newUser) => {
  var userAvailable =  allUser.filter(function(user) {
    return user.id === newUser.id;
  });
  if(userAvailable.length===0){
    return (dispatch)=>{
      allUser.push(newUser);
      dispatch(selectUser(allUser,newUser,false))
    }
  }else{
    var selectedUser = userAvailable[0];
    return (dispatch)=>dispatch(selectUser(allUser,selectedUser,false));
  }
}

export const addMessage = (message)=>{
  return{
    type:ADD_MESSAGES,
    payload:{
      message,
    }
  }
}

export const changeMessageLoader = (loading)=>{
  return{
    type:MESSAGE_LOADER,
    payload:{
      loading,
    }
  }
}

// export const storeTypedMessage = ()=>{

// }

export const sendMessage = (data,selectedConversation)=>{
  return (dispatch)=>{
    var message = {
      id:Math.random(),
      conversation_id:data.conversation_id,
      sender:data.sender,
      type:data.type,
      reciever_id:data.reciever_id,
      message:data.message,
      read:0,
      created_at:new Date(),
      updated_at:new Date(),
      imageUrl:data.imageUrl,
      local:true,
    }
    // var conversation = {
    //   id:data.conversation_id,
    //   last_message:data.message,
    //   partner:selectedUser,
    //   sent_by:data.sender_id,
    //   bubble_color:null,
    //   background_color:null,
    //   background_image:null,
    //   created_at:new Date(),
    //   updated_at:new Date(),
    // }
    selectedConversation.last_message = data.message;
    selectedConversation.sent_by = data.sender.id;
    selectedConversation.last_message_type = data.type;
    dispatch(addMessage(message));
    dispatch(updateConversationList(selectedConversation));
    var pData = new FormData();;
    pData.append("message",data.message);
    pData.append("conversation_id",data.conversation_id);
    Axios.post(url.apiUrl+"message-to-conversation", pData)
    .then(res=>{
    })
    .catch(err=>{
      console.log(err.response);
    })
  }
}

export const sendFirstMessage = (data,selectedConversation) => {
  var selectedUser = selectedConversation.partner;
   return (dispatch)=>{
    var message = {
      id:Math.random(),
      conversation_id:data.conversation_id,
      sender:data.sender,
      type:data.type,
      reciever_id:data.reciever_id,
      message:data.message,
      read:0,
      created_at:new Date(),
      updated_at:new Date(),
      imageUrl:data.imageUrl,
      local:true,
    }
    dispatch(addMessage(message));
     var pdata = new FormData();
     pdata.append("message",data.message);
     pdata.append("to_user_id",data.reciever_id);
    //  var pdata = {
    //   message:data.message,
    //   to_user_id:data.reciever_id,
    //  }
     Axios.post(url.apiUrl+"message",pdata)
     .then(res=>{
       var message = res.data;

       var selectedConversation = {
        id:message.conversation_id,
        last_message:data.message,
        last_message_type:message.type,
        is_group:0,
        seen:0,
        sent_by:global.userId,
        bubble_color:null,
        background_color:null,
        background_image:null,
        partner:selectedUser,
        currentPage:1,
        lastPage:1,
      }

      // var conversation = {
      //   id:message.conversation_id,
      //   last_message:data.message,
      //   partner:selectedUser,
      //   sent_by:global.userId,
      //   bubble_color:null,
      //   background_color:null,
      //   background_image:null,
      //   created_at:new Date(),
      //   updated_at:new Date(),
      // }
       dispatch(addMessage(message));
       dispatch(updateConversationList(selectedConversation));
       dispatch(determineFirstMessage(false));
       dispatch(setMessages([],selectedConversation,selectedConversation,false));
        Axios.get(url.apiUrl+"conversation/"+selectedConversation.id)
          .then(res=>{
            dispatch(replaceLoadedConversation(res.data));
          })
          .catch(err=>{

          })
      })
     .catch(err=>{
       console.log(err);
       console.log(err.response);
     })
   }
}

export const setMessages = (newMessages,newConversation,selectedConversation,loading)=>{
  return{
    type:MESSAGES,
    payload:{
      newMessages,
      newConversation,
      selectedConversation,
      loading,
    }
  }
}

export const  loadMessages = (loadedConversations,newConversation)=>{
  var conversationId = newConversation.id;
  // var partnerId = newConversation.partner.id;
  var conversationAvailable =  loadedConversations.filter(function(conversation) {
    return conversation.id === conversationId;
  });
  if(conversationAvailable.length!==0){
    var selectedConversation = conversationAvailable[0];
    return (dispatch)=>{
      dispatch(setMessages([],null,selectedConversation,false))
      dispatch(determineFirstMessage(false));
    }
  }else{
    return (dispatch)=>{
      dispatch(setMessages([],null,{},true));
      Axios.get(url.apiUrl+"messages-of-conversation/"+conversationId)
      .then(res=>{
        var data = res.data;
        var messages = data.data;
        newConversation.current_page = data.current_page;
        newConversation.last_page = data.last_page;

        // var selectedConversation = {
        //   id:conversationId,
        //   partnerId,
        //   currentPage,
        //   lastPage,
        // }

        var selectedConversation = newConversation;
        dispatch(setMessages(messages,selectedConversation,selectedConversation,false));
        dispatch(determineFirstMessage(false));
      })
      .catch(err=>{
        console.log(err);
        console.log(err.response);
      });
    }
  }
}

export const setSearchdUsers = (allUsers,currentPage,lastPage,loading)=>{
  return{
    type:SEARCH_USER,
    payload:{
      allUsers,
      currentPage,
      lastPage,
      loading,
    }
  }
}

export const setSearchedMemberForGroup = (searchMemberText,searchedMember,searchMemberLoading)=>{
  return{
    type:SEARCH_MEMBER_FOR_GROUP,
    payload:{
      searchMemberText,
      searchedMember,
      searchMemberLoading,
    }
  }
}


export const changeUserSearchText = (searchText)=>{
  return{
    type:CHANGE_SEARCH_USER_TEXT,
    payload:{
      searchText,
    }
  }
}

// export const setMemberAdded = (id)=>{
//   return{
//     type:SET_MEMBER_ADDED,
//     setMemberAdded
//   }
// }

export const addMemberToGroup = (data,state)=>{
  return (dispatch)=>{
    const {text,searchedMember} = state;
    for (var i=0; i<searchedMember.data.length; i++){
      if(searchedMember.data[i].id == data.user_id){
        searchedMember.data[i].added=true;
        break;
      }
    }
    
    dispatch(setSearchedMemberForGroup(text,searchedMember,false));
    var pData={
      conversation_id:data.conversation_id,
      user_id:data.user_id,
    }
    Axios.post(url.apiUrl+"conversation/add-member",pData)
    .then(res=>{
  
    })
    .catch(err=>{
      console.log(err);
      console.log(err.response);
    })
  }
}


export const searchUsers = (text,type)=>{
    return(dispatch)=>{
      if(text){
        if(type=="forGroupMember"){
          dispatch(setSearchedMemberForGroup(text,{},true));
        }else{
          dispatch(changeUserSearchText(text));
          dispatch(setSearchdUsers([],1,1,true));
        }
        Axios.get(url.apiUrl+"search-users/"+text)
        .then(res=>{
          var data = res.data;
          if(type=="forGroupMember"){
            dispatch(setSearchedMemberForGroup(text,data,false));
          }else{
            dispatch(setSearchdUsers(data.data,data.current_page,data.last_page,false));
          }
          
        })
        .catch(err=>{
          dispatch(setSearchdUsers([],1,1,false));
        });
      }else{
        if(type=="forGroupMember"){
          dispatch(setSearchedMemberForGroup("",{},false));
        }else{
          dispatch(changeUserSearchText(""));
          dispatch(setSearchdUsers([],1,1,false));
        }
      }
    }
}

export const selectSearchedUser=(loadedConversations,loadedUsers,user)=>{
  var availableConversation =  loadedConversations.filter(function(conversations){
    if(conversations.partner){
      return conversations.partner.id === user.id;
    }
  });

  var conversationAvailable = availableConversation.length>0;
  return(dispatch)=>{
    dispatch(changeUserSearchText(""));
    dispatch(getUserDetail(loadedUsers,user));
    if(conversationAvailable){
      dispatch(loadMessages(loadedConversations,availableConversation[0]));
    }else{
      dispatch(setMessages([],null,{},true));
      Axios.get(url.apiUrl+"messages-with-user/"+user.id)
      .then(res=>{
        var data = res.data;
        var messages = data.data;
        var currentPage = data.current_page;
        var lastPage = data.last_page;

        var selectedConversation = {
          id:messages[0].conversation_id,
          last_message:messages[0].message,
          last_message_type:messages[0].type,
          seen:0,
          is_group:0,
          sent_by:global.userId,
          bubble_color:null,
          background_color:null,
          background_image:null,
          partner:user,
          currentPage,
          lastPage,
        }
        dispatch(setMessages(messages,selectedConversation,selectedConversation,false));
        dispatch(determineFirstMessage(false));
        Axios.get(url.apiUrl+"conversation/"+selectedConversation.id)
        .then(res=>{
          dispatch(replaceLoadedConversation(res.data));
        })
        .catch(err=>{

        })
        
      })
      .catch(err=>{
        var conversation = {
          id:Math.random(),
          last_message:"",
          last_message_type:"text",
          seen:0,
          is_group:0,
          sent_by:global.userId,
          bubble_color:null,
          background_color:null,
          background_image:null,
          partner:user,
          currentPage:1,
          lastPage:1,
        }
        dispatch(selectFakeConversation(conversation))
        dispatch(determineFirstMessage(true));
        dispatch(changeMessageLoader(false));
      })
    }
  }
}

// replace with the old or the fake loaded or selected conversation data in the message reducer
export const replaceLoadedConversation = (conversation)=>{
  return{
    type:REPLACE_LOADED_CONVERSATION,
    payload:{
      conversation,
    }
  }
}

export const determineFirstMessage = (firstMessage) => {
  return{
    type:DETERMINE_FIRST_MESSAGE,
    payload:{
      firstMessage,
    }
  }
}

export const setMyDetail = (detail)=>{
  return{
    type:SET_MY_DETAIL,
    payload:detail,
  }
}

export const getMyDetail = ()=>{
  return (dispatch)=>{
    dispatch(setMyDetail({loading:true,error:false}));
    Axios.get(url.apiUrl+"user")
    .then(res=>{
      var data = res.data;
      dispatch(setMyDetail({...data,loading:false,error:false}));
    })
    .catch(err=>{
      dispatch(setMyDetail({loading:false,error:true}));
    })
  }
}

export const updateMyDetail = (pdata)=>{
  return(dispatch)=>{
    dispatch(setMyDetail({loading:true,error:false}));
    Axios.post(url.apiUrl+"update-user",pdata)
    .then(res=>{
      var data = res.data;
      AsyncStorage.setItem('@user',JSON.stringify(data));
      dispatch(setMyDetail({...data,loading:false,error:false,editable:false}));
      // cookie.save('user',data);
      // window.location.href = "#/app";
    })
    .catch(err=>{
      console.log(err.response);
      dispatch(setMyDetail({loading:false,error:true}));
    })
  }
}

export const cancelUpdateMyDetail = ()=>{
  return async(dispatch)=>{
    // var user = cookie.load("user");
    var user = await AsyncStorage.getItem('@user');
    user = JSON.parse(user);
    // AsyncStorage.getItem
    dispatch(setMyDetail(user));
    dispatch(setMyDetail({editable:false}));
    // window.location.href = "#/app";
  }
}

export const selectProfilePicture = (image,url)=>{
  return(dispatch)=>{
    dispatch(setMyDetail({selectedProfilePic:image,selectProfilePictureUrl:url}));
  }
}

export const updateProfilePic = (fd)=>{
  console.log (fd);
  return(dispatch)=>{
    dispatch(setMyDetail({ppUploading:true,ppUploadError:false}));
    Axios.post(url.apiUrl+"update-user",fd)
    .then(res=>{
      var data = res.data;
      console.log(data);
      dispatch(setMyDetail({...data,ppUploading:false,error:false,selectedProfilePic:null,selectProfilePictureUrl:null}));
      AsyncStorage.setItem('@user',JSON.stringify(data));
      // cookie.save('user',data);
    })
    .catch(err=>{
      console.log(err);
      console.log(err.response);
      dispatch(setMyDetail({ppUploading:false,ppUploadError:true}));
    })
  }
}

export const setSuggestedUsers = (allUsers,currentPage,lastPage,loading)=>{
  return{
    type:SUGGESTED_USERS,
    payload:{
      allUsers,
      currentPage,
      lastPage,
      loading,
    }
  }
}

export const getSuggestedUser = ()=>{
  return(dispatch)=>{
      dispatch(setSuggestedUsers([],1,1,true));
      Axios.get(url.apiUrl+"suggested-users")
      .then(res=>{
        var data = res.data;
        dispatch(setSuggestedUsers(data.data,data.current_page,data.last_page,false));
      })
      .catch(err=>{
        console.log(err.response);
        dispatch(setSuggestedUsers([],1,1,false));
      });
  }
}
export const showSuggestedUsers = (shown)=>{
  return{
    type:SHOW_SUGGESTED_USERS,
    payload:{
      shown,
    }
  }
}

// method for setting the bubble colors list
export const setBubbleColors = (colors,loading)=>{
  return {
    type:GET_BUBBLE_COLORS,
    payload:{
      colors,
      loading,
    }
  }
}

// method for getting the bubblecolors from the api
export const getBubbleColors=()=>{
  return(dispatch)=>{
    dispatch(setBubbleColors([],true));
    Axios.get(url.apiUrl+"bubble-colors")
    .then(res=>{
      var colors = res.data;
      dispatch(setBubbleColors(colors,false));
    })
    .catch(err=>{
      dispatch(setBubbleColors([],false));
    })
  }
}

export const changeGroupName = (conversation,name)=>{
  return (dispatch)=>{
    var pdata = {
      name
    }
    conversation.name = name;
    var updatedConversation = 
    {
      ...conversation,
      name,
    };
    // conversation.name = name;
    
    dispatch(replaceLoadedConversation(updatedConversation));
    Axios.put(url.apiUrl+"conversation/"+conversation.id,pdata)
    .then(res=>{
      // console.log(res.data);
    })
    .catch(err=>{
      console.log(err.response);
    });
  }
}

export const changeGroupImage = (conversation,image,imageUrl)=>{
  return (dispatch)=>{
    var pdata = new FormData();
    pdata.append("profile_image",image);
    // var pdata = {
    //   profile_image:
    // }
    var updatedConversation = 
    {
      ...conversation,
      profile_image:imageUrl,
      local:true,
    };
    // conversation.name = name;
    
    dispatch(replaceLoadedConversation(updatedConversation));
    Axios.post(url.apiUrl+"conversation/update-profile-picture/"+conversation.id,pdata)
    .then(res=>{
      // console.log(res.data);
    })
    .catch(err=>{
      console.log(err.response);
    });
  }
}

export const changeBubbleColor = (conversation,colorCode)=>{
  return (dispatch)=>{
    var pdata = {
      bubble_color:colorCode,
    }
    var updatedConversation = conversation;
    updatedConversation.bubble_color = colorCode;
    dispatch(replaceLoadedConversation(updatedConversation));
    
    Axios.put(url.apiUrl+"conversation/"+conversation.id,pdata)
    .then(res=>{
    })
    .catch(err=>{
      console.log(err.response);
    });
  }
}

// method for setting the background colors list
export const setBackgroundColors = (colors,loading)=>{
  return {
    type:GET_BACKGROUND_COLORS,
    payload:{
      colors,
      loading,
    }
  }
}

// method for getting the background colors from the api
export const getBackgroundColors=()=>{
  return(dispatch)=>{
    dispatch(setBubbleColors([],true));
    Axios.get(url.apiUrl+"background-colors")
    .then(res=>{
      var colors = res.data;
      dispatch(setBackgroundColors(colors,false));
    })
    .catch(err=>{
      dispatch(setBackgroundColors([],false));
    })
  }
}

export const changeBackgroundColor = (conversation,colorCode)=>{
  return (dispatch)=>{
    var pdata = {
      background_color:colorCode,
    }
    var updatedConversation = conversation;
    updatedConversation.background_color = colorCode;
    dispatch(replaceLoadedConversation(updatedConversation));
    
    Axios.put(url.apiUrl+"conversation/"+conversation.id,pdata)
    .then(res=>{
      // console.log(res.data);
    })
    .catch(err=>{
      console.log(err.response);
    });
  }
}

export const changeCreateGroupStatus = (showGroupCreatorIndex,creatingGroup,gcSuccess,gcError,gcErrorMessage)=>{
  return{
    type:CREATE_GROUP,
    payload:{
      showGroupCreator:showGroupCreatorIndex,
      creatingGroup,
      gcSuccess,
      gcError,
      gcErrorMessage,
    }
  }
}

export const createGroup = (group)=>{
  return(dispatch)=>{
    const {name} = group;
    var pData = {name};
    dispatch(changeCreateGroupStatus(true,true,false,false,""));
    Axios.post(url.apiUrl+"conversation/create-group",pData)
    .then(res=>{
      dispatch(changeCreateGroupStatus(false,false,true,false,""));
      dispatch(updateNewGroupName(""));
      dispatch(showGroupCreator(false));
      dispatch(updateConversationList(res.data));
    })
    .catch(err=>{
      dispatch(changeCreateGroupStatus(true,false,false,true,"Unable to create group"));
    });
  }
}

export const showGroupCreator = (status)=>{
  return{
    type:SHOW_GROUP_CREATOR,
    payload:{
      showGroupCreator:status,
    }
  }
}

export const updateNewGroupName = (name)=>{
  return{
    type:UPDATE_NEW_GROUP_NAME,
    payload:{
      newGroupName:name,
    }
  }
}

export const changeAuthentication = (authenticated,loading)=>{
  return{
    type:CHANGE_AUTHENTICATION,
    payload:{
      authenticated,
      loading,
    }
  }
}

export const clearStore =()=>{
  return{
    type:CLEAR_STORE,
    payload:null,
  }
}

// export const getGroupMembers = (id)=>{
//   Axios.get(url.apiUrl+"conversation/list-members/"+id)
//   .then()
// }





// export const updateBubbleColor=(id,color)=>{
  
// }


// export const setFilter = filter => ({ type: SET_FILTER, payload: { filter } });

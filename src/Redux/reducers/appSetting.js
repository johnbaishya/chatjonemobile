import {CHANGE_APP_SETTING} from "../actionTypes";

const initialState = {
    primaryColor:"#000",
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CHANGE_APP_SETTING: {
      const {key,value} = action.payload;
      return {
        ...state,
        [key]:value
      };
    }
    default:
      return state;
  }
}

import {CHANGE_AUTHENTICATION} from "../actionTypes";

const initialState = {
    authenticated:false,
    loading:true,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CHANGE_AUTHENTICATION: {
      const {authenticated,loading } = action.payload;
      return {
        ...state,
        authenticated,
        loading,
      };
    }
    default:
      return state;
  }
}

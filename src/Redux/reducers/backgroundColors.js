import {GET_BACKGROUND_COLORS} from "../actionTypes";

const initialState = {
  colors:[],
  loading:false,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_BACKGROUND_COLORS: {
      const {colors,loading} = action.payload;
      return {
        ...state,
        colors,
        loading,
      };
    }
    // case DETERMINE_FIRST_MESSAGE: {
    //   const { firstMessage } = action.payload;
    //   return {
    //     ...state,
    //     firstMessage,
    //   };
    // }
    default:
      return state;
  }
}

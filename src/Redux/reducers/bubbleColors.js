import {GET_BUBBLE_COLORS} from "../actionTypes";

const initialState = {
  colors:[],
  loading:false,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_BUBBLE_COLORS: {
      const {colors,loading} = action.payload;
      return {
        ...state,
        colors,
        loading,
      };
    }
    // case DETERMINE_FIRST_MESSAGE: {
    //   const { firstMessage } = action.payload;
    //   return {
    //     ...state,
    //     firstMessage,
    //   };
    // }
    default:
      return state;
  }
}

import {GET_INITIAL_CONVERSATION,UPDATE_CONVERSATION_LIST,CREATE_GROUP,SHOW_GROUP_CREATOR,
  UPDATE_NEW_GROUP_NAME, SEARCH_MEMBER_FOR_GROUP,REPLACE_LOADED_CONVERSATION,CLEAR_STORE} from "../actionTypes";

const initialState = {
   data:[],
   currentPage:1,
   lastPage:null,
   loading:true,
   newGroupStatus:{
    showGroupCreator:false,
    creatingGroup:false,
    gcSuccess:false,
    gcError:false,
    gcErrorMessage:"",
    newGroupName:"",
   },
   searchMemberData:{
    searchedMember:{},
    searchMemberText:"",
    searchMemberLoading:false,
   },
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_INITIAL_CONVERSATION: {
      const {data, currentPage,lastPage,loading} = action.payload;
      return {
        ...state,
        data:state.data.concat(data),
        currentPage,
        lastPage,
        loading
      };
    }
    case UPDATE_CONVERSATION_LIST: {
      const {conversation} = action.payload;
      var allConversation = state.data;
      var newConversation = conversation;
      var conversationIndex = allConversation.findIndex((con)=>{
        return con.id===newConversation.id;
      })
      var conversationList = state.data;
      if(conversationIndex!==-1){
        conversationList.splice(conversationIndex,1);
      }
      conversationList.unshift(newConversation);
      return {
        ...state,
        data:conversationList,
      };
    }
    case CREATE_GROUP:{
      const {showGroupCreator,creatingGroup,gcSuccess,gcError,gcErrorMessage} = action.payload;
      return{
        ...state,
        newGroupStatus:{
          ...state.newGroupStatus,
          showGroupCreator,
          creatingGroup,
          gcSuccess,
          gcError,
          gcErrorMessage,
          
        }
      }
    }
    case SHOW_GROUP_CREATOR:{
      const {showGroupCreator} = action.payload;
      return{
        ...state,
        newGroupStatus:{
          ...state.newGroupStatus,
          showGroupCreator,
        }
      }
    }
    case UPDATE_NEW_GROUP_NAME:{
      const {newGroupName} = action.payload;
      return{
        ...state,
        newGroupStatus:{
          ...state.newGroupStatus,
          newGroupName,
        }
      }
    }
    case SEARCH_MEMBER_FOR_GROUP:{
      const {searchedMember,searchMemberText,searchMemberLoading} = action.payload;
      return{
        ...state,
        searchMemberData:{
          ...state.searchMemberData,
          searchedMember,
          searchMemberText,
          searchMemberLoading,
        }
      }
    }
    case REPLACE_LOADED_CONVERSATION:{
      const {conversation} = action.payload;
      let allConversation = state.data;
      let newConversation = conversation;
      let conversationIndex = allConversation.findIndex((con)=>{
        return con.id===newConversation.id;
      })
      let conversationList = state.data;
      if(conversationIndex!==-1){
        conversationList[conversationIndex] = conversation
      }
      return {
        ...state,
        data:conversationList,
      };
    }
    case CLEAR_STORE:{
      return initialState;
    }
    default:
      return state;
  }
}

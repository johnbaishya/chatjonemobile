import { combineReducers } from "redux";
// import visibilityFilter from "./visibilityFilter";
// import todos from "./todos";
import loginReducer from "./login";
import signupReducer from "./signup";
import conversationReducer from "./conversation";
import getUserReducer from "./user";
import messageReducer from "./messages";
import searchReducer from "./search";
import profileReducer from "./profile";
import suggestedUsersReducer from "./suggestedUsers";
import bubbleColors from "./bubbleColors";
import backgroundColors from "./backgroundColors";
import authenticateReducer from "./authenticate";
import appSettingReducer from "./appSetting";

export default combineReducers({ 
    loginReducer,
    signupReducer,
    conversationReducer,
    getUserReducer,
    messageReducer,
    searchReducer,
    profileReducer,
    suggestedUsersReducer,
    bubbleColors,
    backgroundColors,
    authenticateReducer,
    appSettingReducer
});

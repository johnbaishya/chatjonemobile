import {SET_LOGIN_CREDENTIALS,CLEAR_CREDENTIALS,CLEAR_STORE} from "../actionTypes";

const initialState = {
    email:"",
    password:"",
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_LOGIN_CREDENTIALS: {
      const {email,password } = action.payload;
      return {
        ...state,
        email:email,
        password:password,
      };
    }
    case CLEAR_CREDENTIALS: {
      // const { id } = action.payload;
      return {
        email:"",
        password:"",
      };
    }
    case CLEAR_STORE:{
      return initialState;
    }
    default:
      return state;
  }
}

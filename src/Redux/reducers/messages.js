import {MESSAGES,ADD_MESSAGES, MESSAGE_LOADER,REPLACE_LOADED_CONVERSATION,SELECT_FAKE_CONVERSATION,CLEAR_STORE} from "../actionTypes";

const initialState = {
   allMessages:[],
   loadedConversations:[],
   selectedConversation:{},
   loading:false,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case MESSAGES: {
      const {newMessages, newConversation,selectedConversation,loading} = action.payload;
      return {
        // data:state.data.concat(data),
        allMessages:state.allMessages.concat(newMessages),
        loadedConversations:newConversation?[...state.loadedConversations,newConversation]:state.loadedConversations,
        selectedConversation,
        loading
      };
    }
    case ADD_MESSAGES: {
      const {message } = action.payload;
      return {
        ...state,
        allMessages:[message,...state.allMessages]
      };
    }
    case MESSAGE_LOADER:{
      const {loading} = action.payload;
      return{
        ...state,
        loading,
      }
    }
    case REPLACE_LOADED_CONVERSATION:{
      const {conversation} = action.payload;
      var loadedConversations = state.loadedConversations;
      var selectedConversation = state.selectedConversation;
      var changeSc = selectedConversation.id === conversation.id;
      var indexOfCon = loadedConversations.findIndex(con=>{
        return con.id === conversation.id;
      });
      if(indexOfCon>=0){
        loadedConversations[indexOfCon] = conversation;
      }
      var newSelectedCon = selectedConversation;
      if(changeSc){
        newSelectedCon = conversation;
      }
      return{
        ...state,
        loadedConversations,
        selectedConversation:newSelectedCon,
      }
    }
    case SELECT_FAKE_CONVERSATION:{
      const{conversation} = action.payload;
      return{
        ...state,
        selectedConversation:conversation,
      }

    }
    case CLEAR_STORE:{
      return initialState;
    }
    default:
      return state;
  }
}

import {SET_MY_DETAIL,CLEAR_STORE} from "../actionTypes";

const initialState = {
  id:"",
  name:"",
  phone:"",
  profile_img:"",
  loading:false,
  error:false,
  editable:false,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_MY_DETAIL: {
      const data = action.payload;
      return {
        ...state,
        ...data,
      };
    }
    // case DETERMINE_FIRST_MESSAGE: {
    //   const { firstMessage } = action.payload;
    //   return {
    //     ...state,
    //     firstMessage,
    //   };
    // }
    case CLEAR_STORE:{
      return initialState;
    }
    default:
      return state;
  }
}

import {SEARCH_USER, CHANGE_SEARCH_USER_TEXT,CLEAR_STORE} from "../actionTypes";

const initialState = {
   searchText:"",
   allUsers:[],
   selectedUser:{},
   loading:false,
   currentPage:1,
   lastPage:null,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SEARCH_USER: {
      const {allUsers,loading,currentPage,lastPage} = action.payload;
      return {
        // data:state.data.concat(data),
        ...state,
        allUsers,
        currentPage,
        lastPage,
        loading
      };
    }
    case CHANGE_SEARCH_USER_TEXT: {
      const {searchText } = action.payload;
      return {
        ...state,
        searchText,
      };
    }
    case CLEAR_STORE:{
      return initialState;
    }
    default:
      return state;
  }
}

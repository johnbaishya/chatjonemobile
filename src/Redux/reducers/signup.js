import {SIGNUP_CREDENTIALS,CLEAR_STORE} from "../actionTypes";

const initialState = {
    name:"",
    phone:"",
    email:"",
    password:"",
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SIGNUP_CREDENTIALS: {
      const {name,email,phone,password, } = action.payload;
      return {
        ...state,
        name:name,
        email:email,
        phone:phone,
        password:password,
      };
    }
    case CLEAR_STORE:{
      return initialState;
    }
    default:
      return state;
  }
}

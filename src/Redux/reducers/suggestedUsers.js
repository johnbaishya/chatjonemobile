import {SUGGESTED_USERS,SHOW_SUGGESTED_USERS,CLEAR_STORE} from "../actionTypes";

const initialState = {
   allUsers:[],
   selectedUser:{},
   loading:false,
   currentPage:1,
   lastPage:null,
   shown:false,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SUGGESTED_USERS: {
      const {allUsers,loading,currentPage,lastPage} = action.payload;
      return {
        ...state,
        allUsers,
        currentPage,
        lastPage,
        loading,
      };
    }
    case SHOW_SUGGESTED_USERS: {
      const {shown} = action.payload;
      return {
        ...state,
        shown,
      };
    }
    case CLEAR_STORE:{
      return initialState;
    }
    default:
      return state;
  }
}

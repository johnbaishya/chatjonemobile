import {GET_USER_DETAIL,DETERMINE_FIRST_MESSAGE,CLEAR_STORE} from "../actionTypes";

const initialState = {
  allUser:[],
  selectedUser:{},
  loading:false,
  firstMessage:true,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_USER_DETAIL: {
      const {allUser,selectedUser,loading} = action.payload;
      return {
        ...state,
        allUser,
        selectedUser,
        loading,
      };
    }
    case DETERMINE_FIRST_MESSAGE: {
      const { firstMessage } = action.payload;
      return {
        ...state,
        firstMessage,
      };
    }
    case CLEAR_STORE:{
      return initialState;
    }
    default:
      return state;
  }
}

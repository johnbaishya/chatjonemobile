import React,{useState,useEffect} from "react";
import { connect } from "react-redux";
import {View,Text,FlatList,ActivityIndicator,TouchableOpacity} from "react-native";
import {selectSearchedUser,getSuggestedUser,showSuggestedUsers,searchUsers, changeUserSearchText,addMemberToGroup} from "../../Redux/action";
import colors from "../../colors";
import EachUser from "../../Components/EachUser";
import SearchBar from "../../Components/SearchBar";
import SearcedUsers from "../SearchedUsers";
import Axios from "axios";
import url from "../../url";
import IconF5 from "react-native-vector-icons/FontAwesome5";

const userClicked = (uid,cid,props)=>{
    var pData = {
        conversation_id:cid,
        user_id:uid,
    }
    props.addMemberToGroup(pData,props.searchMemberData);
} 

const AddMembers = (props)=>{
    var sc = props.selectedConversation;
    var btnColor = sc.bubble_color?sc.bubble_color:colors.primary;
    var searchMemberData = props.searchMemberData;
    var searchedMember = searchMemberData.searchedMember;
    var users = searchedMember.data;
    var loading = searchMemberData.searchMemberLoading;
    return(
        <View>
            <View style={{width:"90%", height:50, justifyContent:"center",alignSelf:"center"}}>
            <TouchableOpacity
                onPress={()=>{props.navigation.goBack()}}
            >
                <IconF5 name = "arrow-left" color={btnColor} size={20}/>
            </TouchableOpacity>
            </View>
            <View style={{width:"90%", alignSelf:"center"}}>
                <SearchBar
                 placeholder="search user"
                //  value={searchText}
                 onChangeText = {e=>{props.searchUsers(e,"forGroupMember")}}
                //  showCross={searchText?true:false}
                 />
            </View>
            {
            loading?
                <ActivityIndicator size = {50} color={btnColor} style={{marginTop:50}}/>:
                <FlatList
                    data={users}
                    // extraData={this.state}
                    keyExtractor={(item, index) => item.id.toString()}
                    renderItem={({item,index})=>(
                        <EachUser user={item} 
                            onPress={()=>userClicked(item.id,sc.id,props)}
                        />)}
                />
            }
        </View>
    )
}

const mapStateToProps = (state)=>{
    const {profileReducer,messageReducer,conversationReducer} = state;
    return {
        ...profileReducer,
        selectedConversation:messageReducer.selectedConversation,
        searchMemberData:conversationReducer.searchMemberData,
    };

}

export default connect(
    mapStateToProps,
    {
        selectSearchedUser,
        getSuggestedUser,
        showSuggestedUsers,
        searchUsers, 
        addMemberToGroup,
        changeUserSearchText
    }
    )(AddMembers);
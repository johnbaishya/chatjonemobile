import React from "react";
import {Text,Button,View,StyleSheet,TouchableOpacity,ActivityIndicator} from "react-native";
import {changeAuthentication,logout,sendMessage, sendFirstMessage} from "./../../Redux/action";
import {connect} from "react-redux";
import { GiftedChat,Bubble,Send } from 'react-native-gifted-chat';
import url from "../../url";
import colors from "../../colors";
import ProfilePicture from "./../../Components/ProfilePicture";
import Icon from "react-native-vector-icons/FontAwesome";
import IconF5 from "react-native-vector-icons/FontAwesome5";
import {identifyEmptyObject,getUnique} from "./../../Methods";
import ImagePicker from 'react-native-image-crop-picker';   
import {SuggestedUserLoader} from "../../Components/Loaders";

const UserLoader =()=>{
  return(
    <View style={{width:"85%", marginTop:10}}>
      <SuggestedUserLoader/>
    </View>
  )
}




const sendMessage2 = (input,setInput,type,props)=>{
    var data = {
        conversation_id:props.selectedConversation.id,
        reciever_id:props.selectedConversation.partner?props.selectedConversation.partner.id:null,
        type:type,
        sender:{
            id:global.userId,
            name:global.user.name,
            email:global.user.email,
            phone:global.user.phone,
            profile_img:global.user.profile_img,
        },
        message:type=="image"?input.image:input,
        imageUrl:type=="image"?input.imageUrl:"",
    }
    var selectedConversation = props.selectedConversation;
    var messageLoading = props.messageLoading;
    var scEmpty = Object.keys(selectedConversation).length === 0 && selectedConversation.constructor === Object;
    var conNotEmpty = !scEmpty;
    var messageNotLoading = !messageLoading;
    var notEmpty = true;
    if(type=="text"){
        notEmpty = input.replace(/\s/g, '').length!==0;
    }
    var readyToSendMessge = conNotEmpty&&messageNotLoading&&notEmpty;
    var firstMessage = props.firstMessage;
    if(readyToSendMessge){
        if(!firstMessage){
            props.sendMessage(data,selectedConversation);
        }else{
            props.sendFirstMessage(data,selectedConversation);
        }
        if(setInput){
            setInput("");
        }
    }
}

const onSend = (data,props)=>{
  var message = data[0];
  var input = message.text?message.text:message.image;
  var type = message.text?"text":"image";
  sendMessage2(input,null,type,props);
}

const sendImage = (props,type)=>{
  var pickerType;
  if(type=="camera"){
      pickerType="openCamera"
  }else{
      pickerType="openPicker"
  }
  ImagePicker[pickerType]({
      width: 300,
      height: 300,
      cropping: true,
      compressImageQuality:0.3,
    }).then(image => {
      console.log(image);
      var imgFile = {
          name: "file.jpg",
          type: "image/jpg",
          uri:image.path,
        }
      sendMessage2({image:imgFile,imageUrl:image.path},null,"image",props);
      // var fd = new FormData();
      // fd.append("profile_img",imgFile);
      // props.updateProfilePic(fd);
    });
}

const ChatScreen = (props)=>{
    var navigation = props.navigation;
    var scon = props.selectedConversation;
    var sconEmpty = identifyEmptyObject(scon);
    var bgColor= "#fff";
    var bblColor=colors.primary;
    var mainPP = "";
    var conName = "";
    var messageLoading = props.messageReducer.loading;

    if(!sconEmpty){
        bgColor = scon.background_color;
        bblColor = scon.bubble_color?scon.bubble_color:colors.primary;
        var partner = scon.partner;
        if(partner){
            var pp = scon.is_group==1?scon.profile_image:scon.partner.profile_img;
            mainPP = pp?scon.local?pp:url.baseUrl+pp:"";
            conName = scon.is_group==1?scon.name:scon.partner.name;
            // var mainPP = pp?sc.local?pp:url.baseUrl+pp:"";
        }else{
            var pp = scon.is_group==1?scon.profile_image:scon.partner.profile_img;
            mainPP = pp?scon.local?pp:url.baseUrl+pp:"";
            conName = scon.is_group==1?scon.name:scon.partner.name;
        }
    }
    
    // var conName = scon.is_group==1?scon.name:scon.partner.name;
    
    return(
        <View style={[styles.container,bgColor&&{backgroundColor:bgColor}]}>
        {/* {this.imageViewer()} */}
        <View style={[styles.chatHead,{borderBottomColor:bblColor}]}>
          <TouchableOpacity
            style={styles.leftBox}
            onPress={() => navigation.goBack()}
          >
              <IconF5 name="arrow-left" size={20} color={bblColor}/>
            {/* <Image source={left} style={styles.left}/> */}
          </TouchableOpacity>
          {
            messageLoading?
            <UserLoader/>:
            <React.Fragment>
              <View style={styles.imageBox}>
                {/* {this.state.partner_detail.partner_pic ? <Image source={{ uri: url.MainUrl + 'storage/' + this.state.partner_detail.partner_pic }} style={styles.image} /> : <Image source={pp} style={styles.image} />} */}
                <ProfilePicture src={mainPP} size = {35} isGroup={scon.is_group==1}/>
              </View>
              <View style={styles.nameBox}>
                <View style={styles.name}>
                  <Text style={[styles.nameText,{color:bblColor}]}>{conName}</Text>
                </View>
                {/* <View style={styles.time}>
                  <Text style={styles.timeText}> Last seen {moment(this.state.partner_detail.last_chat).fromNow()} </Text>
                </View> */}
              </View>
              <TouchableOpacity
                style={styles.rightBox}
                onPress={() => navigation.navigate("ConversationDetail")}
              >
                  <Icon name="info-circle" size={30} color={bblColor}/>
                {/* <Image source={left} style={styles.left}/> */}
              </TouchableOpacity>
            </React.Fragment>
          }
        </View>
        {
          messageLoading&&
          <View style={{justifyContent:"center",height:"60%"}}>
          <ActivityIndicator size={50} style={{justifyContent:"center", alignSelf:"center"}} color={colors.primary}/>
          </View>
        }
   {!messageLoading&&
   <GiftedChat
        messages={props.messages}
        onSend={(data)=>onSend(data,props)}
        locale={'gmt+45'}
        user={{
          _id: global.user.id,
        }}
        alwaysShowSend
        renderSend={props => {
            return (
              <Send {...props}>
                <View style={{ marginRight: 10, marginBottom: 12 }}>
                  <Icon name="send" color={bblColor} size={25}/>
                </View>
              </Send>
            );
          }}
          renderActions={() => {
            return (
                <View style={{flexDirection:"row"}}>
                     <TouchableOpacity onPress={()=>{sendImage(props,"camera")}}>
                        <Icon name="camera" color={bblColor} size={25}  style={{ marginLeft: 12, marginBottom: 12 }}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>{sendImage(props,"gallery")}}>
                        <Icon name="photo" color={bblColor} size={25}  style={{ marginLeft: 12, marginBottom: 12 }}/>
                    </TouchableOpacity>
                </View>
             
            );
          }}
        renderUsernameOnMessage={props.selectedConversation.is_group==1}
        renderBubble={props2 => {
            var bubbleColor= props.selectedConversation.bubble_color;
            return (
              <Bubble
                {...props2}
                textStyle={{
                  right: {
                    color: '#fff',
                  },
                  left: {
                    color: '#6d6d6d'
                  }
                }}
                usernameStyle={{
                  color:bubbleColor?bubbleColor:colors.primary,
                }}
                tickStyle={{
                  color: 'white'
                }}
                wrapperStyle={{
                  left: {
                    backgroundColor:colors.light,
                    // left:-45,
                    marginTop:5,
                  },
                  right: {
                    backgroundColor:bubbleColor?bubbleColor:colors.primary,
                    marginTop:5,
                  }
                }}>
                  <Text>hiassdfsd</Text>
                </Bubble>
            )
          }}

      />}
      </View>
    )
}

const mapStateToProps = (state)=>{
    const {authenticateReducer,messageReducer,getUserReducer,profileReducer} = state;
    var loading = messageReducer.loading;
    var firstMessage = getUserReducer.firstMessage;
    var partnerId = getUserReducer.selectedUser.id;
    var filteredMessages;
    // console.log("fist msg", firstMessage);
    if(!firstMessage){
        filteredMessages =  messageReducer.allMessages.filter(function(messages) {
            return messages.conversation_id == messageReducer.selectedConversation.id;
        });
    }else{
        filteredMessages =  messageReducer.allMessages.filter(function(messages) {
            return messages.reciever_id === partnerId;
        });
    }
    
    var messages = filteredMessages;
    messages = getUnique(messages,"id");
    var messagesForgc = [];
    messages.map((item,index)=>{
        var messageType = item.type=="text"?"text":"image";
        var message = item.type=="text"?item.message:item.local?item.imageUrl:url.baseUrl+item.message;
        // data.local?data.imageUrl:url.baseUrl+data.message;
        var pp = item.sender.profile_img;
        var mainpp = pp?pp:"/images/pp.png";

        // local time for nepal
        var mssgDate = item.created_at;
        console.log("before Date",mssgDate);
        if(!item.local){
          mssgDate = new Date(item.created_at.replace(' ', 'T') + "Z");
        }
        console.log("after Date",mssgDate);
        var eachMessage = {
            _id:item.id,
            [messageType]:message,
            createdAt:mssgDate,
            user:{
                _id:item.sender.id,
                name:item.sender.name,
                avatar:url.baseUrl+mainpp,
            }
        };
        messagesForgc.push(eachMessage);
    });
    return{
        messages:messagesForgc,
        authenticateReducer,
        selectedConversation:messageReducer.selectedConversation,
        selectedUser:getUserReducer.selectedUser,
        firstMessage:getUserReducer.firstMessage,
        messageLoading:loading,
        messageReducer,
        profileReducer,
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff'
    },
  
    chatHead: {
      height: 50,
      width: '100%',
      flexDirection: 'row',
      borderBottomWidth:0.5,
      borderBottomColor: '#A58E52',
      backgroundColor:"#fff"
    },
  
    leftBox: {
      marginLeft: '1.5%',
      height: '100%',
      width: '6%',
      justifyContent: 'center',
      alignItems: 'center'
    },
    rightBox:{
      // marginLeft: '1.5%',
      height: '100%',
      // width: '6%',
      justifyContent: 'center',
      alignItems: 'center'
    },
  
    left: {
      height: 16,
      width: 12
    },
  
    imageBox: {
      height: '100%',
      width: '12.5%',
      justifyContent: 'center',
      alignItems: 'center'
    },
  
    image: {
      height: 32,
      width: 32,
      borderRadius: 4,
      backgroundColor: '#A58E52'
    },
    image2: {
      height: 40,
      width: 40,
      borderRadius: 4,
      backgroundColor: '#A58E52'
    },
  
    image3: {
      height: 150,
      width: 150,
      borderRadius: 4,
      backgroundColor: '#A58E52'
    },
    nameBox: {
      height: '100%',
      width: '68.5%',
      justifyContent:"center",
    },
  
    name: {
      height: '50%',
      width: '80%',
      justifyContent: 'flex-end',
    },
  
    nameText: {
      fontSize: 16,
      fontWeight: 'bold',
    },
  
    time: {
      height: '50%',
      width: '81.5%',
      justifyContent: 'flex-start'
    },
  
    timeText: {
      fontSize: 12,
      color: '#A58E52'
    },
    grid: {
      margin: '2%',
      width: '96%',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    button1: {
      width: 90,
      height: 27.8,
      // borderWidth: 1,
      // borderRadius: 4,
      // borderColor: '#a58e52',
      // flexDirection: 'row',
      // justifyContent: 'center',
      // alignItems: 'center'
    },
  
    buttonPress1: {
      width: 50,
      height: 50,
      // borderWidth: 1,
      borderRadius: 50,
      borderColor: '#a58e52',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#a58e52'
    },
  
    //   chat: {
    //     width: '100%',
    //     height: 40
    //   }
  });
  

export default connect(mapStateToProps,{
    changeAuthentication,
    logout,
    sendMessage,
    sendFirstMessage
})(ChatScreen);
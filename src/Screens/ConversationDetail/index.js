import React, { useState } from "react";
import {Text,View,TouchableOpacity,StyleSheet,ScrollView} from "react-native";
import IconF5 from "react-native-vector-icons/FontAwesome5";
import {connect} from "react-redux";
import colors from "../../colors";
import ProfilePicture from "./../../Components/ProfilePicture";
import {identifyEmptyObject} from "./../../Methods";
import url from "./../../url";
import Button from "../../Components/Button";
import Modal, { ModalContent,BottomModal,ModalTitle } from 'react-native-modals';
import ImagePicker from "react-native-image-crop-picker";
import {changeGroupImage} from "../../Redux/action";
import PartnerDetail from "./../../Components/PartnerDetail";
import ConversationOption from "./../../Components/ConversationOptions";
// import {Content,Contaii} from "native-base";
// import 


const OpenImagePicker = (props,showModal,type)=>{
    showModal(false);
    var pickerType;
    if(type=="camera"){
        pickerType="openCamera"
    }else{
        pickerType="openPicker"
    }
    ImagePicker[pickerType]({
        width: 300,
        height: 300,
        cropping: true,
        compressImageQuality:0.3,
      }).then(image => {
        console.log(image);
        var imgFile = {
            name: "file.jpg",
            type: "image/jpg",
            uri:image.path,
          }
          props.changeGroupImage(props.selectedConversation,imgFile,image.path);
      });
  }

const OptionModal = (props,modal,showModal)=>{
    var btnColor = props.detailColor?props.detailColor:colors.primary;
    return(
        <BottomModal
            visible={modal}
            onTouchOutside={()=>showModal(false)}
            modalTitle={<ModalTitle title="Profile Photo" />}
        >
            <ModalContent>
                <View style={{width:"100%", flexDirection:"row", justifyContent:"space-around"}}>
                    <View style={{alignItems:"center"}}>
                        <Button icon="camera" style={{...styles.iconBtn,backgroundColor:btnColor}} 
                        onPress={()=>{OpenImagePicker(props,showModal,"camera")}}
                        />
                        <Text>Camera</Text>
                    </View>
                    <View style={{alignItems:"center"}}>
                        <Button icon="photo" style={{...styles.iconBtn,backgroundColor:btnColor}}
                        onPress={()=>{OpenImagePicker(props,showModal,"gallery")}}
                        />
                        <Text>Gallery</Text>
                    </View>
                </View>
            </ModalContent>
        </BottomModal>
    )
}

const ConversationDetail = (props)=>{
    const [ppModal,setPPModal] = useState(false);
    var navigation = props.navigation;
    var scEmpty = identifyEmptyObject(props.selectedConversation);
    var loading = false;
    if(scEmpty){
      return null;
    }
    var sc = props.selectedConversation;
    // var loading = props.getUserReducer.loading;
    // var currentUser = props.getUserReducer.selectedUser;
    let name = sc.is_group==1?sc.name?sc.name:"New Group":sc.partner.name;
    var pp = sc.is_group==1?sc.profile_image:sc.partner.profile_img;
    var mainPP = pp?sc.local?pp:url.baseUrl+pp:"";
    console.log(mainPP);
    return(
        <React.Fragment>
            <View style={{width:"90%", height:50, justifyContent:"center",alignSelf:"center"}}>
                <TouchableOpacity
                    onPress={()=>{navigation.goBack()}}
                >
                    <IconF5 name = "arrow-left" color={props.detailColor} size={20}/>
                </TouchableOpacity>
            </View>
        <ScrollView>
        <View style={{width:"90%", alignItem:"center", alignSelf:"center",marginBottom:20}}>
                <View  style={{width:"100%", alignItems:"center"}}>
                    <View>
                    <ProfilePicture src={mainPP} size={150}  isGroup={sc.is_group==1}/>
                    {sc.is_group==1&&
                    <React.Fragment>
                        <Button icon="camera" 
                            onPress ={()=>setPPModal(true)}
                            // loading={profile.ppUploading}
                            style={{
                                position:"absolute", 
                                bottom:0, 
                                right:0,
                                alignSelf:"center",
                                backgroundColor:props.detailColor,
                            ...styles.iconBtn
                            }}
                        />
                    {OptionModal(props,ppModal,setPPModal)}
                    </React.Fragment>
                    }
                    </View>
                    <Text style={{fontSize:25, fontWeight:"bold", marginTop:10}}>{name}</Text>
                    <PartnerDetail conversation={sc}/>
                    <ConversationOption {...props}/>
                </View>
        </View>
            </ScrollView>
            </React.Fragment>
    )
}

const mapStateToProps = (state)=>{
    const { getUserReducer,messageReducer } = state;
    var detailColor = messageReducer.selectedConversation.bubble_color;
    var selectedConversation = messageReducer.selectedConversation;
    return { 
        getUserReducer,
        detailColor:detailColor?detailColor:colors.primary,
        selectedConversation,
     };
}

const styles = StyleSheet.create({
    iconBtn:{
        width:50,
        height:50,
        borderRadius:50,
        justifyContent:"center",
    }
})


export default connect(mapStateToProps,{
    changeGroupImage,
})(ConversationDetail);
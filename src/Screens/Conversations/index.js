import React,{useEffect} from "react";
import {Text,Button,View,ActivityIndicator} from "react-native";
import {FlatList} from "react-native-gesture-handler";
import {
    changeAuthentication,
    logout,
    getInitialConversation,
    getUserDetail,
    setInitialConversation,
    loadMessages,
} from "./../../Redux/action";
import colors from "./../../colors";
import {connect} from "react-redux";
import EachConversation from "./../../Components/EachConversation";
import {getUnique} from "./../../Methods";
import GroupCreator from "./../../Components/GroupCreator";
import {UserListLoader} from "../../Components/Loaders";

const conversationClicked = (props,item)=>{
    if(item.is_group==0){
        props.getUserDetail(props.getUserReducer.allUser,item.partner.id);
    }
    props.loadMessages(props.messageReducer.loadedConversations,item);
    props.navigation.navigate("ChatScreen");
}   

const Conversation = (props)=>{
    var navigation = props.navigation;
    var authenticated = props.authenticateReducer.authenticated;

    var noConversationLoaded = props.conversationReducer.data.length===0;
    var conversationData = props.conversationReducer.data;
    conversationData = getUnique(conversationData,'id');
    useEffect(()=>{
        if(noConversationLoaded){
            props.getInitialConversation();
        }
    },[]);
    return(
        <View>
            {
               props.conversationReducer.loading&&
                <UserListLoader/>
            }
            <FlatList
                data={conversationData}
                // extraData={this.state}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={({item,index})=>(<EachConversation data={item} onPress={()=>{conversationClicked(props,item)}}/>)}
            />
            {!props.conversationReducer.loading&&<GroupCreator/>}
            {/* <Text style={{marginTop:100}}>Conversation Page</Text>
            <Button
                title={"go to chat"}
                onPress={() =>navigation.navigate("ChatScreen")}
            />
            <Button
                title={"Logout"}
                onPress={() =>props.logout()}
            /> */}
        </View>
    )
}

const mapStateToProps = (state)=>{
    const {authenticateReducer,conversationReducer,getUserReducer,messageReducer} =state;
    return{
        authenticateReducer,
        conversationReducer,
        getUserReducer,
        messageReducer
    }
}

export default connect(mapStateToProps,{
    changeAuthentication,
    logout,
    getInitialConversation,
    getUserDetail,
    setInitialConversation,
    loadMessages,
})(Conversation);
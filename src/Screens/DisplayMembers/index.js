import React,{useState,useEffect} from "react";
import { connect } from "react-redux";
import {View,Text,FlatList,ActivityIndicator,TouchableOpacity} from "react-native";
import {selectSearchedUser,getSuggestedUser,showSuggestedUsers,searchUsers, changeUserSearchText} from "./../../Redux/action";
import colors from "../../colors";
import EachUser from "../../Components/EachUser";
import SearchBar from "../../Components/SearchBar";
import SearcedUsers from "./../SearchedUsers";
import Axios from "axios";
import url from "./../../url";
import IconF5 from "react-native-vector-icons/FontAwesome5";

const getGroupMembers  = (id,setLoading,setMembers)=>{
    setLoading(true);
    Axios.get(url.apiUrl+"conversation/list-members/"+id)
    .then(res=>{
        var data = res.data;
        setMembers(data);
        setLoading(false);
    })
    .catch(err=>{
        setLoading(false);
        console.log(err);
    })
}

const Users = (props)=>{
    const [members,setMembers] = useState([]);
    const [loading,setLoading] = useState(false);
    var sc = props.selectedConversation;
    useEffect(()=>{
        getGroupMembers(sc.id,setLoading,setMembers);
    },[props.conId]);
    var btnColor = sc.bubble_color?sc.bubble_color:colors.primary;
    return(
        <View>
            <View style={{width:"90%", height:50, justifyContent:"center",alignSelf:"center"}}>
            <TouchableOpacity
                onPress={()=>{props.navigation.goBack()}}
            >
                <IconF5 name = "arrow-left" color={btnColor} size={20}/>
            </TouchableOpacity>
            </View>
            {
            loading&&
                <ActivityIndicator size = {50} color={btnColor} style={{marginTop:50}}/>
            }
            <FlatList
                data={members}
                // extraData={this.state}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={({item,index})=>(<EachUser user={item}/>)}
            />
        </View>
    )
}

const mapStateToProps = (state)=>{
    const {profileReducer,messageReducer} = state;
    return {
        ...profileReducer,
        selectedConversation:messageReducer.selectedConversation,
    };

}

export default connect(
    mapStateToProps,
    {selectSearchedUser,getSuggestedUser,showSuggestedUsers,searchUsers, changeUserSearchText}
    )(Users);
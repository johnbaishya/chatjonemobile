import React, { useState } from "react";
import {Text,View,Image,TouchableOpacity,Keyboard} from "react-native";
import {changeAuthentication,setCrendentials,setMyDetail} from "./../../Redux/action";
import {connect} from "react-redux";
import Input from "./../../Components/Input";
import colors from "../../colors";
import logo from "./../../Images/logo.png";
import Button from "./../../Components/Button";
import AsyncStorage from '@react-native-community/async-storage';
import url from "./../../url";
import Axios from "axios";

const loginMethod = (props,setLogging,setErrorMessage)=>{
    Keyboard.dismiss();
    setLogging(true);
    setErrorMessage(null);
    const {email,password} = props.loginReducer;
    var fd = new FormData();
    fd.append("email", email);
    fd.append("password", password);
    Axios.post(url.apiUrl+"login",fd)
    .then(res=>{
        var {token,user} = res.data;
        AsyncStorage.setItem('@token',token);
        AsyncStorage.setItem('@user',JSON.stringify(user));
        global.userId = user.id;
        global.user = user;
        props.setMyDetail(user);
        Axios.defaults.headers.common = {
            'Authorization': 'Bearer ' + token,
            'Accept': "application/json",
            'Content-Type': "application/json"
          }
        setLogging(false);
        setErrorMessage(null);
        props.changeAuthentication(true,false);
        props.setCrendentials({
            email:"",
            password:""
        })
    })
    .catch(err=>{
        setLogging(false);
        setErrorMessage("Unknown Error");
        if(err.response){
            if(err.response.status){
                if(err.response.status === 401){
                    setErrorMessage("Email or password is invalid");
                }
            }
        }
    })
}

const updateInputs = (props,key,data)=>{
    props.setCrendentials({
        ...props.loginReducer,
        [key]:data,
    })
}

const Login = (props)=>{
    const [logging,setLogging] = useState(false);
    const [errorMessage,setErrorMessage] = useState(null);
    var navigation = props.navigation;
    var authenticated = props.authenticateReducer.authenticated;
    const {email,password} = props.loginReducer;
    return(
        <View style={{backgroundColor:colors.light, height:"100%", width:"100%"}}>
            <View style={{width:"80%", alignSelf:"center", alignItems:"center",justifyContent:"center",height:"100%"}}>
                {/* <Text style={{marginTop:100}}>Login Page</Text> */}
                <Image source={logo} style={{height:100, width:150}} resizeMode="contain" />
                <Input
                    placeholder = "Email"
                    type="email-address"
                    onChangeText = {(text)=>{updateInputs(props,"email",text)}}
                    value = {email}
                />
                <Input
                    placeholder = "Password"
                    password = {true}
                    onChangeText = {(text)=>{updateInputs(props,"password",text)}}
                    value = {password}
                />
                {errorMessage&&
                <Text style={{color:"red"}}>
                    {errorMessage}
                </Text>}
                <Button
                    title={"Login"}
                    // onPress={() => authenticated?props.changeAuthentication(false,false):props.changeAuthentication(true,false)}
                    onPress={()=>loginMethod(props,setLogging,setErrorMessage)}
                    full
                    loading={logging}
                />
                {/* <Text>
                    or
                </Text> */}
                <View style={{height:"2%"}}/>
                <TouchableOpacity
                    onPress={() => navigation.navigate('Signup')}
                >
                    <Text style={{fontSize:15, color:colors.primary,textDecorationLine:"underline"}}>Create new Account</Text>
                </TouchableOpacity>
                
            </View>
        </View>
    )
}

const mapStateToProps = ({authenticateReducer,loginReducer})=>{
    return{
        authenticateReducer,
        loginReducer
    }
}

export default connect(mapStateToProps,{
    changeAuthentication,
    setCrendentials,
    setMyDetail
})(Login);
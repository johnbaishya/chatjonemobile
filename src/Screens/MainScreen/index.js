import React from "react";
import {Text,Button,View,Image} from "react-native";
import {changeAuthentication,logout} from "./../../Redux/action";
import {connect} from "react-redux";
import {Container, Header, Content,Tab, Tabs,Body,Title,TabHeading,Icon,Left} from "native-base";
import Conversation from "./../Conversations";
import ChatScreen from "./../Chatscreen";
import Users from "./../Users";
import Settings from "./../Settings";
import logo from "./../../Images/logo.png";
import colors from "./../../colors";


const MainScreen = (props)=>{
    var navigation = props.navigation;
    var authenticated = props.authenticateReducer.authenticated;
    return(
        // <View>
        //     <Text style={{marginTop:100}}>Main Screen Page</Text>
        //     <Button
        //         title={"go to chat"}
        //         onPress={() =>navigation.navigate("ChatScreen")}
        //     />
        //     <Button
        //         title={"Logout"}
        //         onPress={() =>props.logout()}
        //     />
        // </View>
        <Container>
            <Header hasTabs>
                <Left>
                    <Image source={logo} style={{width:40}} resizeMode="contain"/>
                </Left>
                <Body>
                    <Title>Chatjone</Title>
                </Body>
            </Header>
            <Tabs tabBarPosition="overlayTop">
                <Tab heading={<TabHeading><Icon name="ios-chatbubbles" /></TabHeading>}
                 activeTextStyle={{color:colors.primary}} 
                 textStyle={{color:colors.lightText}}
                 >
                    <Conversation {...props}/>
                </Tab>
                <Tab heading={<TabHeading><Icon name="ios-contacts" /></TabHeading>}  
                    activeTextStyle={{color:colors.primary}} 
                    textStyle={{color:colors.lightText}}>
                    <Users {...props}/>
                </Tab>
                <Tab heading={<TabHeading><Icon name="md-settings" /></TabHeading>}  
                    activeTextStyle={{color:colors.primary}} 
                    textStyle={{color:colors.lightText}}>
                    <Settings {...props}/>
                </Tab>
            </Tabs>
        </Container>
    )
}

const mapStateToProps = ({authenticateReducer})=>{
    return{
        authenticateReducer,
    }
}

export default connect(mapStateToProps,{changeAuthentication,logout})(MainScreen);
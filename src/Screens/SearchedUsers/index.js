import React,{useState,useEffect} from "react";
import { connect } from "react-redux";
import {View,Text,FlatList,ActivityIndicator} from "react-native";
import {selectSearchedUser,getSuggestedUser,showSuggestedUsers} from "./../../Redux/action";
import colors from "../../colors";
import EachUser from "../../Components/EachUser";
import SearchBar from "../../Components/SearchBar";

const userClicked=(props,item)=>{
    var loadedConversation = props.loadedConversations;
    var user = item;
    props.selectSearchedUser(loadedConversation,props.loadedUsers,user);
    props.showSuggestedUsers(false);
    props.navigation.navigate('ChatScreen');
}

const SearchedUsers = (props)=>{
    var loading = props.searchReducer.loading;
    var allUsers = props.searchReducer.allUsers;

    return(
        <React.Fragment>
            {
               loading&&
                <ActivityIndicator size = {50} color={colors.primary} style={{marginTop:50}}/>
            }
            <FlatList
                data={allUsers}
                // extraData={this.state}
                keyExtractor={(item, index) => item.id.toString()}
                renderItem={({item,index})=>(<EachUser user={item} onPress={()=>userClicked(props,item)}/>)}
            />
        </React.Fragment>
    )
}

const mapStateToProps = (state) => {
    const { searchReducer,messageReducer,getUserReducer} = state;
    return {
        searchReducer,
        loadedConversations:messageReducer.loadedConversations,
        loadedUsers:getUserReducer.allUser,
    };
  };

export default connect(
    mapStateToProps,
    {selectSearchedUser,getSuggestedUser,showSuggestedUsers}
    )(SearchedUsers);
import React,{useState} from "react";
import {Text,View,StyleSheet} from "react-native";
import {changeAuthentication,logout,selectProfilePicture,updateProfilePic} from "./../../Redux/action";
import {connect} from "react-redux";
import ProfilePicture from "./../../Components/ProfilePicture";
import url from "../../url";
import Button from "../../Components/Button";
import Detail from "./../../Components/Detail";
import { Container, Content } from "native-base";
import Modal, { ModalContent,BottomModal,ModalTitle } from 'react-native-modals';
import ImagePicker from 'react-native-image-crop-picker';   


const selectFromgallery = (props,showModal,type)=>{
    var pickerType;
    if(type=="camera"){
        pickerType="openCamera"
    }else{
        pickerType="openPicker"
    }
    showModal(false);
    ImagePicker[pickerType]({
        width: 300,
        height: 300,
        cropping: true,
        compressImageQuality:0.3,
      }).then(image => {
        var imgFile = {
            name: "file.jpg",
            type: "image/jpg",
            uri:image.path,
          }
      
        var fd = new FormData();
        fd.append("profile_img",imgFile);
        props.updateProfilePic(fd);
      });
}


const OptionModal = (props,modal,showModal)=>{
    return(
        <BottomModal
            visible={modal}
            onTouchOutside={()=>showModal(false)}
            modalTitle={<ModalTitle title="Profile Photo" />}
        >
            <ModalContent>
                <View style={{width:"100%", flexDirection:"row", justifyContent:"space-around"}}>
                    <View style={{alignItems:"center"}}>
                        <Button icon="camera" style={styles.iconBtn} onPress={()=>{selectFromgallery(props,showModal,"camera")}}/>
                        <Text>Camera</Text>
                    </View>
                    <View style={{alignItems:"center"}}>
                        <Button icon="photo" style={styles.iconBtn} onPress={()=>{selectFromgallery(props,showModal,"gallery")}}/>
                        <Text>Gallery</Text>
                    </View>
                </View>
            </ModalContent>
        </BottomModal>
    )
}

const Settings = (props)=>{
    var navigation = props.navigation;
    var authenticated = props.authenticateReducer.authenticated;
    var profile = props.profileReducer;
    var pp = profile.profile_img;
    const [modal,showModal] = useState(false);

    return(
        <Container>
            <Content>
        <View style={{width:"90%", alignSelf:"center", alignItems:"center",paddingTop:30, paddingBottom:30}}>
            <View>
                <ProfilePicture src={pp?url.baseUrl+pp:""} size={150}/>
                <Button icon="camera" 
                    onPress ={()=>showModal(true)}
                    loading={profile.ppUploading}
                    style={{
                        position:"absolute", 
                        bottom:0, 
                        right:0,
                        alignSelf:"center",
                       ...styles.iconBtn
                    }}
                />
            </View>
            <Detail/>
            {/* <Text style={{
                marginTop:5, 
                fontSize:20, 
                fontWeight:"bold"
                }}>{global.user.name}</Text> */}
            <Button
                title={"Logout"}
                onPress={() =>{
                    props.logout(global.user.id);
                }}
                full
                // danger
            />
        </View>
        </Content>
        {OptionModal(props,modal,showModal)}
        </Container>
    )
}

const mapStateToProps = ({authenticateReducer,profileReducer})=>{
    return{
        authenticateReducer,
        profileReducer
    }
}
const styles = StyleSheet.create({
    iconBtn:{
        width:50,
        height:50,
        borderRadius:50,
        justifyContent:"center",
    }
})

export default connect(mapStateToProps,{
    changeAuthentication,
    logout,
    selectProfilePicture,
    updateProfilePic,
})(Settings);
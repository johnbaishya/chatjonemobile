import React, { useState } from "react";
import {Text,View,Image,TouchableOpacity,Dimensions,StatusBar} from "react-native";
import {changeAuthentication,setSignupCredentials,setMyDetail} from "./../../Redux/action";
import {connect} from "react-redux";
import Input from "./../../Components/Input";
import colors from "../../colors";
import logo from "./../../Images/logo.png";
import Button from "./../../Components/Button";
import { ScrollView } from "react-native-gesture-handler";
import url from "././../../url";
import AsyncStorage from '@react-native-community/async-storage';
import Axios from "axios";
const HEIGHT = Dimensions.get("screen").height;
const statsBarHeight = StatusBar.currentHeight;
const realScreenHeight = HEIGHT- statsBarHeight;

const allInputFilled = (props)=>{
    const {name,email,phone,password} = props.signupReducer;
    var notName = !name;
    var notEmail = !email;
    var notPhone = !phone;
    var notPassword = !password;

    if(notName||notEmail||notPhone||notPassword){
        return false;
    }else{
        return true;
    }
}

const updateInputs = (props,key,data)=>{
    props.setSignupCredentials({
        ...props.signupReducer,
        [key]:data,
    })
}

const signupMethod = (props, setSigning, setErrorMessage,setSuccessMessage)=>{
    if(allInputFilled(props)){
        setSigning(true);
        setErrorMessage(null);
        const {email,password,phone,name} = props.signupReducer;
        var fd = new FormData();
        fd.append("email", email);
        fd.append("password", password);
        fd.append("name", name);
        fd.append("phone", phone);
        Axios.post(url.apiUrl+"register",fd)
        .then(res=>{
            var {token,user} = res.data;
            AsyncStorage.setItem('@token',token);
            AsyncStorage.setItem('@user',JSON.stringify(user));
            global.user = user;
            global.userId = user.id;
            props.setMyDetail(user);
            Axios.defaults.headers.common = {
                'Authorization': 'Bearer ' + token,
                'Accept': "application/json",
                'Content-Type': "application/json"
              }
            setSuccessMessage("Account created Successfully");
            setErrorMessage(null);
            // this.props.clearCrendials(1);
            setTimeout(()=>{
                props.changeAuthentication(true,false);
                setSigning(false);
                setSuccessMessage(null);
            }, 1000);
        })
        .catch(err=>{
            setSigning(false);
            setSuccessMessage(null);
            setErrorMessage("unknown error");
            console.log(err.response);
            if(err.response){
                if(err.response.data){
                    var errData = err.response.data;
                    var allError = errData.errors;
                    var errorArray = Object.values(allError);
                    var firstError = errorArray[0][0];
                    setErrorMessage(firstError);
                }else{
                    setErrorMessage("an unknown error occured while creating account");
                }
            }else{
                setErrorMessage("an unknown error occured while creating account");
            }
        })
    }else{
       setErrorMessage("All fields are required * ");
    }
}

const Signup = (props)=>{
    var navigation = props.navigation;
    var authenticated = props.authenticateReducer.authenticated;
    const {name,email,phone,password} = props.signupReducer;
    const [signing, setSigning] = useState(false);
    const [errorMessage,setErrorMessage] = useState(null);
    const [successMessage,setSuccessMessage] = useState(null);
    return(
        <ScrollView style={{height:HEIGHT}}>
        <View style={{backgroundColor:colors.light, height:realScreenHeight, width:"100%"}}>
            <View style={{width:"80%", alignSelf:"center", alignItems:"center",justifyContent:"center",height:"100%"}}>
                {/* <Text style={{marginTop:100}}>Login Page</Text> */}
                <Image source={logo} style={{height:80, width:150}} resizeMode="contain"/>
                <Input
                    placeholder = "Full Name"
                    value = {name}
                    onChangeText = {(e)=>{updateInputs(props,"name",e)}}
                />
                <Input
                    placeholder = "Email"
                    type="email-address"
                    value={email}
                    onChangeText = {(e)=>{updateInputs(props,"email",e)}}
                />
                <Input
                    placeholder = "Phone"
                    type="phone-pad"
                    value={phone}
                    onChangeText = {(e)=>{updateInputs(props,"phone",e)}}
                />
                <Input
                    placeholder = "Password"
                    password = {true}
                    value={password}
                    onChangeText = {(e)=>{updateInputs(props,"password",e)}}
                />
                {errorMessage&&
                    <Text style={{color:"red"}}>{errorMessage}</Text>
                }
                {successMessage&&
                    <Text style={{color:"green"}}>{successMessage}</Text>
                }
                <Button
                    title={"Signup"}
                    onPress={() => signupMethod(props,setSigning,setErrorMessage,setSuccessMessage)}
                    loading = {signing}
                    full
                />
                <View style={{height:"2%"}}/>
                <TouchableOpacity
                    onPress={() => navigation.navigate('Login')}
                >
                    <Text style={{fontSize:15, color:colors.primary, textDecorationLine:"underline"}}>Login with existing account</Text>
                </TouchableOpacity>
            </View>
        </View>
        </ScrollView>
    )
}

const mapStateToProps = ({authenticateReducer,signupReducer})=>{
    return{
        authenticateReducer,
        signupReducer
    }
}

export default connect(mapStateToProps,{
    changeAuthentication,
    setMyDetail,
    setSignupCredentials
})(Signup);
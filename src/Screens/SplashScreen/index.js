import React from "react";
import {View,Text,Image,ActivityIndicator} from "react-native";
import logo from "./../../Images/logo.png";
import loader from "./../../Images/loader.gif";
import colors from "./../../colors";
const SplashScreen = ()=>{
    return(
        <View style={{alignItems:"center", justifyContent:"center",height:"100%"}}>
            <Image source={logo} style={{width:"80%",height:"30%"}} resizeMode="center"/>
            <Text style={{fontWeight:"bold", fontSize:20}}>Chatjone</Text>
            {/* <ActivityIndicator color={colors.primary} size={50}/> */}
        </View>
    )
}

export default SplashScreen;
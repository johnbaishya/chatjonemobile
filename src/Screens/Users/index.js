import React,{useState,useEffect} from "react";
import { connect } from "react-redux";
import {View,Text,FlatList,ActivityIndicator} from "react-native";
import {selectSearchedUser,getSuggestedUser,showSuggestedUsers,searchUsers, changeUserSearchText} from "./../../Redux/action";
import colors from "../../colors";
import EachUser from "../../Components/EachUser";
import SearchBar from "../../Components/SearchBar";
import SearcedUsers from "./../SearchedUsers";
import {SuggestedUserListLoader} from "../../Components/Loaders";

const userClicked=(props,item)=>{
    var loadedConversation = props.loadedConversations;
    var user = item;
    props.selectSearchedUser(loadedConversation,props.loadedUsers,user);
    props.showSuggestedUsers(false);
    props.navigation.navigate('ChatScreen');
}

const Users = (props)=>{
    var noUsersLoaded = props.SuggestedUsers.allUsers.length===0;
    var searchText=props.searchReducer.searchText;
    useEffect(()=>{
        if(noUsersLoaded){
            props.getSuggestedUser();
        }
    },[]);
    var loading = props.SuggestedUsers.loading;
    var allUsers = props.SuggestedUsers.allUsers;
    console.log("st", searchText);
    return(
        <View>
            <View style={{width:"90%", alignSelf:"center"}}>
                <SearchBar
                 placeholder="search user"
                 value={searchText}
                 onChangeText = {e=>{props.searchUsers(e,"forConversation")}}
                 showCross={searchText?true:false}
                 />
            </View>
                {searchText?
                    <SearcedUsers {...props}/>:
                    <React.Fragment>
                    {
                    loading&&
                        // <ActivityIndicator size = {50} color={colors.primary} style={{marginTop:50}}/>
                        <SuggestedUserListLoader/>
                    }
                    <FlatList
                        data={allUsers}
                        // extraData={this.state}
                        keyExtractor={(item, index) => item.id.toString()}
                        renderItem={({item,index})=>(<EachUser user={item} onPress={()=>userClicked(props,item)} />)}
                    />
                </React.Fragment>
                }
           
        </View>
    )
}

const mapStateToProps = (state) => {
    const { searchReducer,messageReducer,getUserReducer,suggestedUsersReducer} = state;
    return {
        searchReducer,
        loadedConversations:messageReducer.loadedConversations,
        loadedUsers:getUserReducer.allUser,
        SuggestedUsers:suggestedUsersReducer,
    };
  };

export default connect(
    mapStateToProps,
    {selectSearchedUser,getSuggestedUser,showSuggestedUsers,searchUsers, changeUserSearchText}
    )(Users);
import firebase, { Notification, RemoteMessage } from 'react-native-firebase';
import {Platform} from "react-native";
import Axios from "axios";
import url  from './../url';
// add following code to an appropriate place.
// implement notifications as needed

const BuildChannnelForAndroiid = ()=>{
  if(Platform.OS==='android'){
    const channel = new firebase.notifications.Android.Channel('message', 'Message', firebase.notifications.Android.Importance.Max)
        .setDescription('Channel to recieve message notification');

      // Create the channel
      firebase.notifications().android.createChannel(channel);
  }
}


const sendTokenToServer = (token)=>{
  var fd = new FormData();
  fd.append('device_token',token);
  Axios.post(url.apiUrl+"device-token",fd);
}

export default  setNotification= async ()=>{
    if (Platform.OS === 'android') {
        try {
          BuildChannnelForAndroiid();
          const res = await firebase.messaging().requestPermission();
          const fcmToken = await firebase.messaging().getToken();
          if (fcmToken) {
            console.log('FCM Token: ', fcmToken);
            global.deviceToken = fcmToken;
            sendTokenToServer(fcmToken);
            const enabled = await firebase.messaging().hasPermission();
            if (enabled) {
              console.log('FCM messaging has permission:' + enabled)
            } else {
              try {
                await firebase.messaging().requestPermission();
                console.log('FCM permission granted')
              } catch (error) {
                console.log('FCM Permission Error', error);
              }
            }
            // firebase.notifications().onNotificationDisplayed((notification: Notification) => {
            //   // Process your notification as required
            //   // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
            //   console.log('Notification display: ', notification)
            // });
            
            // firebase.notifications().onNotification((notification: Notification) => {
            //   console.log('on Notification: ', notification)
            // });
          } else {
            console.log('FCM Token not available');
          }
        } catch (e) {
          console.log('Error initializing FCM', e);
        }
      }

      firebase.notifications().onNotification((notification: Notification) => {
        console.log(notification);
      });
}

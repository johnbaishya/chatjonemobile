const colors = {
    // theme first (light grey header)
    // ===============================================
    primary:'#000',
    light:'#d8d8d8',
    btnDisabled:'#808080',
    danger:'red',
    success:'green',
    info:'blue',
    headerBackground:'#d8d8d8',
    title:"#000",
    lightText:'#808080',
    borderColor:'#808080',
    headerLogo:"#000",
    activeTabIcon:"#000",
    inactiveTabIcon:"#444444",
    // =================================================

    // theme second (black header )
    // primary:'#000',
    // light:'#d8d8d8',
    // btnDisabled:'#808080',
    // danger:'red',
    // success:'green',
    // info:'blue',
    // headerBackground:'#000',
    // title:"#fff",
    // lightText:'#808080',
    // borderColor:'#808080',
    // headerLogo:"#fff",
    // activeTabIcon:"#fff",
    // inactiveTabIcon:"#dadada",
}

export default colors;